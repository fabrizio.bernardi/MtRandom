#!/usr/bin/perl
 
 ##############################################################################
 #                                                                            #
 # Make fft of the syntetic seismograms and put into one file for variance    #
 # analysis.                                                                  #
 #                                                                            #
 ##############################################################################

 use Env;
 use strict;
 use Math::FFT;
 use Math::VecStat qw(max min maxabs minabs sum average);
 use Math::Complex;
 use File::Path;
 use Array::Unique;
 
 ##############################################################################
 # Arguments
 (my $yyyy, my $stlib, my $dept) = @ARGV;
 if($#ARGV != 2) {
   print "Usage: $0 year stations_file.inn depth\n";
   print "       $0 1917 1917_station_file.inn 5\n";
   exit;
 }
 my $z = sprintf("%03s",$dept);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200

 ##############################################################################
 # Paths
 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $mtp   = "/Users/fabrizio/MTs/lib";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";

 ##############################################################################
 # constants for fft
 my $NmaxPoints = 1024*2;;
 my $tpi = 2.*acos(-1.0);
 my $tpi = 1;
 my $pi = 4 * atan2(1, 1);
 my $nor = 1;
 my $om;
 my @lin;
 my $T;

 ##################################################################
 # Open and Read staz lib, and MT-work
#open(STA,"<$stp/$stlib"); my @sta=<STA>; close(STA);
 open(STA,"<$stlib"); my @sta=<STA>; close(STA);
 opendir(DIR,"$wrkp"); my @dir=readdir(DIR); close(DIR);

 ##################################################################
 # Select Staz and paths
 my @alldirs = GetAllDirs($dic,$z,@dir);

 ##################################################################
 # Make new name of dir for spectra
 my @newdirs = MakenewDirs(@alldirs);

 ##################################################################
 # Begin loop over dirs. For each dir:
 # 1. list files
 # 2. make new name
 # 3. check if newdir exist
 # -begin loop over file
 #   4. read files
 #   5. make fft
 #   6. normalize and write in array
 #   7. write sp files
 # - end loop
 # End
 #
 for(my $i=0; $i<=$#alldirs; $i++) {

    # 1. list files
#   my @files = ReadFileList($alldirs[$i],$allstaz[$i]);
    my @files = ReadFileList($alldirs[$i]);

    # 2. make new name
    my @newfi = MakeNewNameFiles(@files);

    # 3. check if newdir exist, if not, create.
    #    only for $alldirs[$i] with $i=0;
    mkpath( "$newdirs[$i]",  {verbose =>0});
    print "$newdirs[$i]\n"; 

    # begin loop over files
    for(my $k=0; $k<=$#files; $k++) {

       # 4. read files
       (my $dtd,my @syn) = ReadSac($alldirs[$i],$files[$k]);

       # array to vector
       my $syn = \@syn;
       
       # 5. make fft
       my $syn_fft   = new Math::FFT($syn);
       my $syn_spctr = $syn_fft->spctrm;

       # Set frequencies and periods
       my $dom   = 1/($NmaxPoints*$dtd); 
       
       # 6. Normalize and write in array 
       my $max = max($syn_spctr);
       next if($max==0); 
       @lin = ();
       for(my $i = 1; $i<$#syn/2; $i++) {
          $syn_spctr->[$i] = $syn_spctr->[$i]*$nor/$max; 
          $om = $dom * $i * $tpi;
          $T  = 1/$om;
          my $a = sprintf("%5d  %e  %e  %e",$i,$om,$T,$syn_spctr->[$i]);
          push(@lin,$a);
       } 
 
       # 7. write sp files
       open(OU,">$newdirs[$i]/$newfi[$k]");
          print OU "       Freq[Hz]      T[s]          PwSpr\n";
          foreach $_ (@lin) {
             print OU "$_\n";
          }
       close(OU);

    }
    # end loop over files    


 } # end loop over dirs and and prog


 ##################################################################
 ##################################################################

 sub ReadSac {

     my $pa = shift(@_);
     my $st = shift(@_);
     my @ou = ();
     open(FH,"<$pa/$st") or die; binmode(FH);
     my $npts  = ReadValue(79,4,'i');
     my $delta = ReadValue(0,4,'f');
     if($npts<$NmaxPoints) {
        for(my $i=158; $i<=158+$npts; $i++) {
           my $a=ReadValue($i,4,'f');
           push(@ou,$a);
        }
        for(my $i=0; $i<=$NmaxPoints-$npts-2; $i++) {
           push(@ou,0);
        }
     }
     if($npts>=$NmaxPoints) {
        for(my $i=158; $i<=158+$NmaxPoints-1; $i++) {
           my $a=ReadValue($i,4,'f');
           push(@ou,$a);
        }

     }
     return ($delta,@ou);

 }

 sub ReadValue {

   seek(FH, @_[0]*@_[1],0);
   read(FH, my $tmp, @_[1]);
   $tmp=unpack(@_[2], $tmp);
   return $tmp;

 }


 sub MakeNewNameFiles {

     my @in = @_;
     for(my $i=0; $i<=$#in; $i++) {
        my @f = split('\.',$in[$i]);
           $f[$#f] = "sp";
        $in[$i] = join('.',@f);
     }
     return @in;

 }

 sub ReadFileList {

     my $pa = shift(@_);
#    my $st = shift(@_);
     my @ou = ();
     opendir(DIR,"$pa"); my @dir=readdir(DIR), close(DIR);
     for(my $i=0; $i<=$#dir; $i++) {
        chomp($dir[$i]);
        my @f = split(/\./,$dir[$i]);
        next if("$dir[$i]" eq ".");
        next if("$dir[$i]" eq "..");
        # conditions
        next if("$f[$#f-2]" ne "$z");
        next if("$f[$#f-0]" ne "co");
#       next if("$f[0]" ne "$st");
        push(@ou,$dir[$i]);
     }

     return @ou;
 }


 ##################################################################


 sub MakenewDirs {

     my @in = @_;
     for(my $i=0; $i<=$#in; $i++) {
        my @f = split('\_',$in[$i]); 
           $f[$#f] = "sp";
        $in[$i] = join('_',@f);
     }
     return @in;

 }


 sub GetAllStaz {

     my @dir = @_;
     my @all = ();

     foreach $_ (@dir) {
        chomp($_);
        my @tmp = split('',$_);
        if("$tmp[0]" eq "0" && "$tmp[1]" eq "0" && $tmp[2] == $dept) {
        opendir(SBD,"$wrkp/$_"); my @sbd=readdir(SBD); close(SBD);
        for(my $j=0; $j<=$#sbd; $j++) {
           next if("$sbd[$j]" eq ".");
           next if("$sbd[$j]" eq "..");
           next if("$sbd[$j]" eq ".DS_Store");
           my @ttt = split(/\_/,$sbd[$j]);
           push(@all,"$ttt[0]");   
       }
    }
  }

  return (@all);
 }

 sub GetAllDirs {

     my $dic = shift(@_);
     my $z   = shift(@_);
     my @dir = @_;
     my @all = ();

     foreach $_ (@dir) {
        chomp($_);
        if("$_" eq "$z") {
#       my @tmp = split('',$_);
#       next if($#tmp!=$dic);
#       if("$tmp[0]" eq "0" && "$tmp[1]" eq "0" && $tmp[2] == $dept) {
#       print "$_\n";
        opendir(SBD,"$wrkp/$_"); my @sbd=readdir(SBD); close(SBD);
        for(my $j=0; $j<=$#sbd; $j++) {
           next if("$sbd[$j]" eq ".");
           next if("$sbd[$j]" eq "..");
           next if("$sbd[$j]" eq ".DS_Store");
           my @f = split(/\_/,$sbd[$j]);
           next if("$f[$#f]" ne "co");

           push(@all,"$wrkp/$_/$sbd[$j]");   
       }
    }
  }

  return (@all);

 } 

