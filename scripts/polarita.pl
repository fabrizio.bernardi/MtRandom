#!/usr/bin/perl

 #################################################################


 use Env;
 use strict;
 use Math::Trig;

 #################################################################
 # path and lib
 my $lib_path = "/Users/fabrizio/MTs/lib";
 my $mtlib = "mtlib_3.1_05000.lib";
 

 open(MT,"<$lib_path/$mtlib"); my @mt=<MT>; close(MT);

 
 my $nrmt = 1;
 my $rd   = 45.0/atan(1.0);
 my $aoi  =   9.6383;
 my $azi  = 0;
 
 my @mt_elements = getMtElements($nrmt);
 

 my $zp_pol              = getVertPolarity($aoi,$azi,@mt_elements);
 (my $np_pol,my $ep_pol) = getOrizPolarity($azi,$zp_pol);
 print "$zp_pol $np_pol $ep_pol\n";



 #################################################################
 #################################################################

 sub getOrizPolarity {

     (my $azi, my $z) = @_;
      my $n;
      my $e;

      if($azi==0 || $azi==90 || $azi==180 || $azi==270 || $azi==360) {
         $n=0;
         $e=0;
      }
      if($azi>0 && $azi<90) {
         $n=$z;
         $e=$z;
      }
      if($azi>90 && $azi<180) {
         $n=-$z;
         $e=$z;
      }
      if($azi>180 && $azi<270) {
         $n=-$z;
         $e=-$z;
      }
      if($azi>270 && $azi<360) {
         $n=$z;
         $e=-$z;
      }

      return ($n,$e);

 }

 sub getVertPolarity {

     my $aoi   = shift(@_);
     my $theta = shift(@_);
     my $mzz = $_[0];
     my $mxx = $_[1];
     my $myy = $_[2];
     my $mxz = $_[3];
     my $myz = $_[4];
     my $mxy = $_[5];
     my $P;

     #compute Polarity 
     my $x = cos($theta/$rd)*sin($aoi/$rd);
     my $y = sin($theta/$rd)*sin($aoi/$rd);
     my $z = cos($aoi/$rd);
     my $polar = $x*$mxx*$x   + 
                 2*$x*$mxy*$y + 
                 2*$x*$mxz*$z + 
                 2*$y*$myz*$z +
                 $y*$myy*$y   +
                 $z*$mzz*$z;

     $P = -1 if($polar<0);
     $P =  1 if($polar>0);
     $P =  0 if($polar==0);


     return $P;

 }

 sub getMtElements {

     my $nr = shift(@_);

     chomp($mt[$nr*10+3]);
     my @l = split(' ',$mt[$nr*10+3]);
     shift(@l);
    
     return @l;

 }
