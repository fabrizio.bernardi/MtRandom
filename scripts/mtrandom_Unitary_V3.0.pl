#!/usr/bin/perl

 ###############################################################
 #                                                             #
 # This script generate a random seismic moment tensor         #
 #                                                             #
 # Constraints: Iso=0                                          #
 #              Eps<=Eps_max                                   #
 #                                                             #
 # Version 1.0      Fb. 19.12.2005                             #
 # Version 2.0      Fb. 03.01.2006                             #
 # Version 2.1      Fb. 13.02.2007                             #
 #                  size range                                 #
 #                                                             #
 ###############################################################

 use Math::Cephes::Matrix;
 use Math::Random;

 ($mode)=@ARGV;
 if($#ARGV != 1) {
      print "Usage: mtrandom mode epsilon_max\n\n";
      print "       mode: s[small] m[medium] l[large] a[all]\n\n";
      exit;
 }
 if("$mode" ne "s" && "$mode" ne "m" && "$mode" ne "l" && "$mode" ne "a") { 
      print "Usage: mtrandom mode\n\n";
      print "       mode: s[small] m[medium] l[large] a[all]\n\n";
      exit;
 }

 #SINCE WE WANT MT WITH EPSILON<=epsilon_max WE REJECT ALL OTHER
 #SOLUTIONS.
 $Epsilon=0.501;
 $epsilon_max=0.500;
 $epsilon_max=$ARGV[1];
 while($Epsilon>$epsilon_max) {
 
 ###############################################################
 # Set arrays for elements and exponent of Mo
 #           |1 4 5|
 # M= Mo*exp |0 2 6|
 #           |0 0 3|
 @trace=(0,1,2);
 @nontr=(3,4,5);
 @SMT=(0,0,0,0,0,0);

 ###############################################################
 # Set mode
 @expon=(18,19,20,21,22,23,24) if("$mode" eq "s");
 @expon=(24,25,26,27,28) if("$mode" eq "m");
 @expon=(26,27,28,29,30) if("$mode" eq "l");
 @expon=(18,19,20,21,22,23,24,25,26,27,28,29) if("$mode" eq "a");

 ###############################################################
 # --- Define Mo: scalar and exponent ------------------------ #
 srand (time ^ $$ ^ unpack "%L*", `ps axww | gzip -f`);
 $mo=rand();                                 # Scalar
 $exponent = 0;
#$exponent=$expon[rand @expon];              # Exponent
 $CO=sprintf("%10.4e",$mo*10**$exponent);    # Combine -> Mo

 ###############################################################
 # --- Set the trace of the MT. Assign a random value to 2 --- #
 # --- randomly selected element of @trace. The third derive - #
 # --- from constrain tr=0.                                    # 
 @trace=MakeTrace($CO,@trace);

 ###############################################################
 # --- Now set random values for @nontr elements  ------------ #
 # --- here no need of constraints --------------------------- #
 @nontr=MakeNontr($CO,@nontr);

 ###############################################################
 # --- Build MTS from @trace and @nontr ---------------------- #
 @SMT[0]=@trace[0];
 @SMT[1]=@trace[1];
 @SMT[2]=@trace[2];
 @SMT[3]=@nontr[0];
 @SMT[4]=@nontr[1];
 @SMT[5]=@nontr[2];

 ###############################################################
 # --- Find eigenvalues and eigenvectors --------------------- #
 # --- From eigenvalues, we set Mo and epsilon --------------- #
 @Eigens=GetEigenValAndVec(@SMT);

 ###############################################################
 # --- Compute Mo and epsilon -------------------------------- #
 # --- The first three elements of @Eigens are the eigenvalues #
 # --- T-axe=max; P-axe=min; N-axe=mean ---------------------- #
 # --- The three eigenvectors follow
 @Axes=(@Eigens[0],@Eigens[1],@Eigens[2]);
 @Axes= sort { $a <=> $b } @Axes;
 ($Paxe,$Naxe,$Taxe)=@Axes; 
 $Paxe=sprintf("%10.4e",$Paxe);
 $Taxe=sprintf("%10.4e",$Taxe);
 $Naxe=sprintf("%10.4e",$Naxe);
 @Axes=($Paxe,$Naxe,$Taxe);
 @Moeps=(abs($Paxe),abs($Naxe),abs($Taxe));
 @Moeps= sort { $a <=> $b } @Moeps;

#$Mo=sprintf("%10.4e",(abs($Paxe)+abs($Taxe))/2);
#$Epsilon=sprintf("%.3f",@Moeps[0]/@Moeps[2]);
 $Mo=(abs($Paxe)+abs($Taxe))/2;
 $Mw=((log($Mo)/log(10))/1.5)-10.73;
 $Epsilon=@Moeps[0]/@Moeps[2];


 } #END OF THE WHILE LOOP
#@SMT=(-1.9247e+25,4.0378e+24,1.5209e+25,6.7952e+24,8.7840e+24,2.6839e+25);
#@SMT=(0,0,0,0,0,1);
#@SMT=(0.237E+23,0.120E+23,-0.357E+23,-0.109E+24,-0.112E+24,-0.875E+23);
 @Eigens=GetEigenValAndVec(@SMT);

 ###############################################################
 # --- Compute directions of the principal axes -------------- #
 # --- This could be done with the previous part (evaluation - #
 # --- of the module of the axis, but...                     - #
 @ax0=(@Eigens[0],@Eigens[3],@Eigens[4],@Eigens[5]);
 @ax1=(@Eigens[1],@Eigens[6],@Eigens[7],@Eigens[8]);
 @ax2=(@Eigens[2],@Eigens[9],@Eigens[10],@Eigens[11]);
 @di0=ComputeDirAxe(@ax0);
 @di1=ComputeDirAxe(@ax1);
 @di2=ComputeDirAxe(@ax2);

 ###############################################################
 # --- Set T(max),N(middle),P(min) axes ---------------------- #
 # --- OK, this is very poor programming, but its late, ------ #
 # --- I am tired, and it WORKS!! ---------------------------- #
 if   (@di0[0]>=@di1[0] && @di0[0]>=@di2[0]) {
      if(@di1[0]>=@di2[0]) {@TAX=@di0;@NAX=@di1;@PAX=@di2}
      else                 {@TAX=@di0;@NAX=@di2;@PAX=@di1}
 }
 elsif(@di1[0]>=@di0[0] && @di1[0]>=@di2[0]) {
    if(@di0[0]>=@di2[0]) {@TAX=@di1;@NAX=@di0;@PAX=@di2}
    else                 {@TAX=@di1;@NAX=@di2;@PAX=@di0}
 }
 elsif(@di2[0]>=@di0[0] && @di2[0]>=@di1[0]) {
    if(@di0[0]>=@di1[0]) {@TAX=@di2;@NAX=@di0;@PAX=@di1}
    else                 {@TAX=@di2;@NAX=@di1;@PAX=@di0}
 }

 ###############################################################
 # --- Sort Eigenvectors with respect to the max Eigenvalue -- #
 @SortedEigens=SortEigens(@Eigens);


 ###############################################################
 # --- Compute fault plane (Strike, dip, slip) --------------- #
 @Planes=ComputePlanes(@SortedEigens);
 for($i=0;$i<=5;$i++) {@Planes[$i]=0.0 if(@Planes[$i]==-0)}


 ###############################################################
 # --- Convert refernce system (r,t,f)->(x,y,z) -------------- #
 # --- xx yy xy yz xz zz ------------------------------------- #         
 @XYZ=(@SMT[0],@SMT[1],@SMT[2],@SMT[3],-@SMT[4],-@SMT[5]);

 ###############################################################
 # --- Output ------------------------------------------------ #
 #Print #Mt elements
 print "Mrtf:  rr          tt          ff          rt          rf          tf\n";
 print "Mxyz:  zz          xx          yy          xz          yz          xy\n";
 print "     ";
 for($i=0;$i<=5;$i++) {
   printf("%12.4e",@SMT[$i]);
 }
 print "\n";
 print "     ";
 for($i=0;$i<=5;$i++) {
   printf("%12.4e",@XYZ[$i]);
 }
 print "\n";

 #Print #Mo,Mw and epsilon
 print "Mo[dyn*cm]= ";
 printf("%10.4e",$Mo);
 print "      Mw= ";
 printf("%3.1f",$Mw);
 print "      Epsilon= ";
 printf("%5.3f",$Epsilon);
 print "\n";

 #Print Planes
 print "Plane1 (Str/Dip/Slp) ";
 printf("%5.1f%6.1f%7.1f",@Planes[0],@Planes[1],@Planes[2]);
 print "\nPlane2               ";
 printf("%5.1f%6.1f%7.1f",@Planes[3],@Planes[4],@Planes[5]);
 print "\n";

 #Print Axes
 print "T-axe (Val/Plg/Azi) ";
 printf("%11.4e%6.1f%7.1f",@TAX[0],@TAX[1],@TAX[2]);
 print "\nN-axe               ";
 printf("%11.4e%6.1f%7.1f",@NAX[0],@NAX[1],@NAX[2]);
 print "\nP-axe               ";
 printf("%11.4e%6.1f%7.1f",@PAX[0],@PAX[1],@PAX[2]);
 print "\n";
 
################################################################
##                       SUB ROUTINES                          #
################################################################

 sub SortEigens {

   my $T = Math::Cephes::Matrix->new([[@_[0],@_[3],@_[4],@_[5]],   
                                      [@_[1],@_[6],@_[7],@_[8]],
                                      [@_[2],@_[9],@_[10],@_[11]],
                                      [0,    0,    0,     0]]);
   my $Zer = Math::Cephes::Matrix->new([[0,    0,    0,     0],   
                                        [0,    0,    0,     0],
                                        [0,    0,    0,     0],
                                        [0,    0,    0,     0]]);
   my $M = $T->coef;
   my $Z = $Zer->coef;

   if($M->[0]->[0]>=$M->[1]->[0] && $M->[0]->[0]>=$M->[2]->[0]) {
     if($M->[1]->[0]>=$M->[2]->[0]) {
                                    for($i=0;$i<=3;$i++) {$Z->[0]->[$i]=$M->[0]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[1]->[$i]=$M->[1]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[2]->[$i]=$M->[2]->[$i]}
     }
     else{
                                    for($i=0;$i<=3;$i++) {$Z->[0]->[$i]=$M->[0]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[1]->[$i]=$M->[2]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[2]->[$i]=$M->[1]->[$i]}
     }
   }
   elsif($M->[1]->[0]>=$M->[0]->[0] && $M->[1]->[0]>=$M->[2]->[0]) {
     if($M->[0]->[0]>=$M->[2]->[0]) {
                                    for($i=0;$i<=3;$i++) {$Z->[0]->[$i]=$M->[1]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[1]->[$i]=$M->[0]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[2]->[$i]=$M->[2]->[$i]}
     }
     else{
                                    for($i=0;$i<=3;$i++) {$Z->[0]->[$i]=$M->[1]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[1]->[$i]=$M->[2]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[2]->[$i]=$M->[0]->[$i]}
     }
   }
   elsif($M->[2]->[0]>=$M->[0]->[0] && $M->[2]->[0]>=$M->[1]->[0]) {
     if($M->[0]->[0]>=$M->[1]->[0]) {
                                    for($i=0;$i<=3;$i++) {$Z->[0]->[$i]=$M->[2]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[1]->[$i]=$M->[0]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[2]->[$i]=$M->[1]->[$i]}
     }
     else{
                                    for($i=0;$i<=3;$i++) {$Z->[0]->[$i]=$M->[2]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[1]->[$i]=$M->[1]->[$i]}
                                    for($i=0;$i<=3;$i++) {$Z->[2]->[$i]=$M->[0]->[$i]}
     }
   }

   @SortEigens=($Z->[0]->[0],$Z->[1]->[0],$Z->[2]->[0]);
   @SortEigens=(@SortEigens,$Z->[0]->[1],$Z->[0]->[2],$Z->[0]->[3]);
   @SortEigens=(@SortEigens,$Z->[1]->[1],$Z->[1]->[2],$Z->[1]->[3]);
   @SortEigens=(@SortEigens,$Z->[2]->[1],$Z->[2]->[2],$Z->[2]->[3]);
   return @SortEigens;


 }

 sub ComputePlanes {

     my $radian=57.29577951;
     my $hsq2=.70710678;
     my $T = Math::Cephes::Matrix->new([[@_[3],@_[6],@_[9]],
                                        [@_[4],@_[7],@_[10]],
                                        [@_[5],@_[8],@_[11]]]);  
     my $M = $T->coef;

     $sgn=-1;
     #Begin loop over 2 planes PL1=j0, PL2=j1
     for ($j=0;$j<=1;$j++) {
       $sgn=0-$sgn; 
       #Begin loop over three eivectors i
       for ($i=0;$i<=2;$i++) {
         $rn->[$i] = (sqrt(2)/2)*($M->[$i]->[0]+$sgn*$M->[$i]->[2]);
         $rn->[$i] = 0 if ($rn->[$i]<=1e-8 && $rn->[$i]>=-1e-8);
         $e->[$i] = (sqrt(2)/2)*($M->[$i]->[0]-$sgn*$M->[$i]->[2]);
         $e->[$i] = 0 if ($e->[$i]<=1e-8 && $e->[$i]>=-1e-8);
       }
       $CheckPolariy=0;
       if($rn->[0]<=0) {
         $CheckPolariy=1;
         for ($i=0;$i<=2;$i++) {
             $rn->[$i] = 0 - $rn->[$i];
             $e->[$i] = 0 - $e->[$i];
         }
       }
       $sind=sqrt($rn->[1]*$rn->[1]+$rn->[2]*$rn->[2]);
       $del->[$j]=atan2($sind,$rn->[0])*$radian;
       $gg=-$rn->[1];
       $hh=-$rn->[2];
       $phs->[$j]=atang2(-$rn->[1],-$rn->[2])*$radian+180;
       $CheckPolariy=0;
       $rlam->[$j]=atang2($e->[0],$rn->[1]*$e->[2]-$rn->[2]*$e->[1])*$radian;
     }
     return $phs->[0],$del->[0],$rlam->[0],$phs->[1],$del->[1],$rlam->[1];

 }

 sub atang2 {  # This subroutine is needed tu put atan2 in the right half-space
               # in case of -0,X

     my $atang2=atan2(@_[0],@_[1]);
     if(@_[0]==-0) {
       $atang2=atan2(-0.0,@_[1]);
     }
     return $atang2;

 }

 sub ComputeDirAxe {

     my $radian=57.29577951;
     my $module=shift(@_);
     my @vector=@_;
     my $pl=atan2(-(@vector[0]),
                  sqrt(@vector[1]**2+@vector[2]**2))*$radian;
     my $az=atan2(-(@vector[2]),@vector[1])*$radian+180;
     
     $module=sprintf("%10.4e",$module);     
     $pl=sprintf("%4.1f",$pl);
     $az=sprintf("%4.1f",$az);
     return ($module,$pl,$az);

 }


 sub GetEigenValAndVec {

     my @tmp=@_;

     my $S = Math::Cephes::Matrix->new([[@tmp[0],@tmp[3],@tmp[4]],
                                        [@tmp[3],@tmp[1],@tmp[5]],
                                        [@tmp[4],@tmp[5],@tmp[2]]]);
     my ($E, $EV1) = $S->eigens();
     my $EV = $EV1->coef;
     my @EIVAL=();
     my @EIVEC=();

     for (my $i=0; $i<3; $i++) {
        $aa=$E->[$i];        
        push(@EIVAL,$aa);
        my $v = [];
        my $sum=0;
        $v->[0] = $EV->[$i]->[0];
        $v->[1] = $EV->[$i]->[1];
        $v->[2] = $EV->[$i]->[2];
        $sum=$sum+$v->[0]*$v->[0]+$v->[1]*$v->[1]+$v->[2]*$v->[2];
        $sum=1/sqrt($sum);
        $sum=0-$sum if($v->[0] >0);
        $v->[0] = $v->[0]*$sum;
        $v->[1] = $v->[1]*$sum;
        $v->[2] = $v->[2]*$sum;
        push(@EIVEC,$v->[0]);
        push(@EIVEC,$v->[1]);
        push(@EIVEC,$v->[2]);
     }
     @tmp=();
     push(@tmp,@EIVAL); 
     push(@tmp,@EIVEC); 
     return @tmp;

 }


 sub MakeNontr {

   my $MM = shift(@_); 
   my @tra = @_;
   my $signtr1 = GetSign(0); 
   my $signtr2 = GetSign(0); 
   my $signtr3 = GetSign(0); 
   @tra[0]=sprintf("%10.4e",rand()*$MM*$signtr1);
   @tra[1]=sprintf("%10.4e",rand()*$MM*$signtr2);
   @tra[2]=sprintf("%10.4e",rand()*$MM*$signtr3);
   return @tra;

 }


 sub MakeTrace {

  my $MM = shift(@_);
  my @tra = @_;
  my $ele_tr1=0;
  my $ele_tr2=0;
  while($ele_tr1==$ele_tr2) {
    $ele_tr1=$tra[rand @tra];
    $ele_tr2=$tra[rand @tra];
  }
  # GetSign
  my $signtr1 = GetSign(0); 
  my $signtr2 = GetSign(0); 
  $TR1=sprintf("%10.4e",rand()*$MM*$signtr1);       #Find random values for
  $TR2=sprintf("%10.4e",rand()*$MM*$signtr1);       #for the trace
  $TR3=sprintf("%10.4e",(0-($TR1+$TR2)));  #constrain tr=0
  $SUM=$TR1+$TR2+$TR3;
  @tra=($TR3,$TR3,$TR3);                   #Assign TR3 to all
  @tra[$ele_tr1]=$TR1;                     #Overwrite the other
  @tra[$ele_tr2]=$TR2;                     #2 random values
  
  return @tra;

 }

   sub GetSign {

      my $in = shift(@_);
      while ($in==0 || $in > 1.5 || $in < -1.5) {
         $in  = random_normal(1,0,1);  
         $in += 0.5 if($in >=0 && $in<0.5);
         $in -= 0.5 if($in <=0 && $in>-0.5);
      }
      $in = sprintf("%.0f",$in);
      return $in

  }

