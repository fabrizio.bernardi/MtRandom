#!/usr/bin/perl

 ##############################################################################
 #                                                                            #
 # Combine Varinace of powerspectra and polarity of P waves                   #
 #                                                                            #
 ##############################################################################

 use Env;
 use strict;
 use Math::FFT;
 use Math::VecStat qw(max min maxabs minabs sum average);
 use Math::Complex;
 use File::Path;
 use Array::Unique;
 use Statistics::Basic qw(:all);

 ##############################################################################
 # Arguments
 (my $yyyy, my $oblib, my $dept, my $varfile, my $stationfile) = @ARGV;
 if($#ARGV != 4) {
   print "Usage: $0 year observed_file.inn depth var_file stati_file.inn\n";
   print "       $0 1917 1917_0426A.inn 5 1917_0426A.var 1917_station_file.inn\n";
   exit;
 }
 my $z = sprintf("%03s",$dept);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200
 my $Tm; my $TM;
 my @variances;   # here al variances for single station and component
                  # structure: Sta_comp_NrMT[0-5000]
 my $NrMTRandom=5000;

 ##############################################################################
 # Paths
 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $mtp   = "/Users/fabrizio/MTs/lib";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";
 my $obsf  = "/Users/fabrizio/MTs/work/$yyyy/observed";

 ##############################################################################
 # Define outfiles
 # fit detailed for each station
 (my $out_01, my $out_02) = DefineOutFiles($oblib);
 print "$out_01\n";
 print "$out_02\n";
#print "$wrkp/$z/$out_01\n";
#print "$wrkp/$z/$out_02\n";
 # open outfile
 open(O1,">$out_01");
 open(O2,">$out_02");
#open(O1,">$wrkp/$z/$out_01");
#open(O2,">$wrkp/$z/$out_02");


 ##################################################################
 # Open and Read staz lib, and MT-work, and observed file, observed path
 open(OBS,"<$oblib"); my @obs=<OBS>; close(OBS);
 open(VAR,"<$varfile"); my @var=<VAR>; close(VAR);
 open(STS,"<$stationfile"); my @sts=<STS>, close(STS);
#open(OBS,"<$stp/$oblib"); my @obs=<OBS>; close(OBS);
#open(VAR,"<$wrkp/$z/$varfile"); my @var=<VAR>; close(VAR);
#open(STS,"<$stp/$stationfile"); my @sts=<STS>, close(STS);
 opendir(DIR,"$wrkp/$z"); my @dir=readdir(DIR); close(DIR);
 opendir(ODI,"$obsf"); my @odi=readdir(ODI); close(ODI);

 ##################################################################
 # Select Staz and paths with spectra: output of do_04
 my @alldirs = GetAllDirs2($dic,$z,@dir);
#my @allpols = GetAllPoll(@alldirs);

 #################################################################
 my $date_event = $obs[0];
 my $surs_event = $obs[1];

 #################################################################
 # Set variances array
 my @MtNr;   #Number MT-random
 my @Vari;   #Variance power spectrum
 my @Stde;   #Standard deviation power spectrum
 my @VARI;   #VAriance all;
 foreach $_ (@var) {
   chomp($_);
   my @l = split(' ',$_);
   push(@MtNr,$l[0]);
   push(@Vari,$l[3]);
   push(@Stde,$l[2]);
#  push(@VARI,$l[3]);
 }

 #################################################################
 # Now for each station and componet read observed polarities to be 
 # combined with variance of power spectra
 my @PolVar;
 my @PolStat;
 my @obspol=();
 my @synpol=();
 my @synpolfile=();
 my @synpolpath=();
 for(my $i=2; $i<=$#obs; $i++) {
 
    chomp($obs[$i]);
    my @lin = split(' ',$obs[$i]);
              # lin[0]     = staz
              # lin[1]     = chan
              # lin[2]     = spin staz
              # lin[3]     = spin z
              # lin[4]     = spin n
              # lin[5]     = spin e
              # lin[6]     = diff phase z unused 
              # lin[7]     = diff phase n unused 
              # lin[8]     = diff phase e unused
              # lin[9-10]  = Min_Max T z
              # lin[11-12] = Min_Max T n
              # lin[13-14] = Min_Max T e
              # lin[15]    = spin polarity
              # lin[16-18] = polarity z n e
  
    
    # this lines now useles, this command shifted below where elements_of_fit
    # are analyzed: if $lin[16]==0 polarity of that station now included in alaysis
    # but printed into 01 output file (better for post processing check)
    #next if($lin[16]==0);  #next if station is off for polarity analysis

    # Pol file to open 
    my $PolToOpen=$lin[0].".pol";
    push(@synpolfile,$PolToOpen);
    # pol path to oopen
    my @p = grep {/$lin[0]/} @alldirs;
#   print ">>>>> @alldirs $PolToOpen\n";
    push(@synpolpath,$p[0]);

    # Read polariites 
    for(my $k=0; $k<=3; $k++) {  #loop over components
      push(@obspol,$lin[16+$k]);
    }

 }

 #################################################################
 # loop over @obspool and then over MT pol and *var file
 # then compute vector
 my @elements_of_fit;

 # first read weigths of pol and var
 chomp($obs[0]);
 my @l = split(' ',$obs[0]);
 my $WPwp = $l[9];
 my $WPol = $l[10];

 for(my $j=0; $j<=$#synpolfile; $j++) {

    # open file
    open(SYN,"<$synpolpath[$j]/$synpolfile[$j]"); my @syn=<SYN>; close(SYN);

    # station:
    my @s = split(/\./,$synpolfile[$j]);
    my $sta = $s[0];
#   print "$synpolpath[$j]/$synpolfile[$j] oooo @s ooo $sta @syn\n";

    # read azimuth and aoi 
    my @gg = grep {/$sta/} @sts;
    chomp($gg[0]);
    my @G = split(' ',$gg[0]);
    my $azi = sprintf("%8.4f",$G[6]);
    my $aoi = sprintf("%6.3f",$G[10]);

    # set obs polarity
    my $sp = shift(@obspol);
    my $pz = shift(@obspol);
    my $pn = shift(@obspol);
    my $pe = shift(@obspol);

    # loop over MTnumber, thus over *.var elements
    for(my $k=0; $k<=$#var; $k++) {

       # read variance values
       chomp($var[$k]);
       my @ele = split(' ',$var[$k]);
       my $nr = $ele[0];
       my $va = $ele[3];
       my $st = $ele[2];

       # read syn pol
       my $pole = shift(@syn);
       chomp($pole);
       my @L = split(' ',$pole);
       my $Pz= $L[1];
       my $Pn= $L[2];
       my $Pe= $L[3];
       ### OLD
#      my $le = shift(@syn);
#      my $ln = shift(@syn);
#      my $lz = shift(@syn);
#      chomp($le); chomp($ln); chomp($lz);
#      my @L = split(' ',$le);
#      my $Pe= $L[1];
#      my @L = split(' ',$ln);
#      my $Pn= $L[1];
#      my @L = split(' ',$lz);
#      my $Pz= $L[1];

       # make for each MT the following string
       # sta_va_st_pe_Pe_pn_Pn_pz_PZ   
       # where Px are synt-polarity and px are obs-polarity
       # Push into array @elements_of_fits.
#      my $vec = join('_',$nr,$sta,$va,$st,$pz,$pn,$pe,$Pz,$Pn,$Pe);
       my $vec = join('_',$nr,$sta,$va,$st,$sp,$pz,$pn,$pe,$Pz,$Pn,$Pe,$azi,$aoi);
       push(@elements_of_fit,$vec);

    }

 }
 
 #################################################################
 # Now make fit
 my @vec_fit;
 @elements_of_fit = sort {$a <=> $b} @elements_of_fit;
 
 for(my $i=0; $i<=$#var; $i++) {
    my $c=0; # polarity that fit
    my $C=0; # polarity to analyze in observed
    my $var_psp;
    my $std_psp;
    my $st_ou;
    for(my $k=0; $k<=$#synpolfile; $k++) {
      my $tmp = shift(@elements_of_fit);
      my @l   = split(/\_/,$tmp);
      my $aoi = pop(@l);
      my $azi = pop(@l);
      # read var and std
      $var_psp = $l[2] if($k==0);
      $std_psp = $l[3] if($k==0);
      # count polarity which fits
      if($l[4] != 0) {
        $c++ if($l[5]*$l[4] == $l[8]);    
        $c++ if($l[6]*$l[4] == $l[9]);    
        $c++ if($l[7]*$l[4] == $l[10]);    
        $C++ if($l[5] != 0);  #8 9 10
        $C++ if($l[6] != 0); 
        $C++ if($l[7] != 0); 
      }

      #out VP file
      $l[2] = $azi;
      $l[3] = $aoi;
      $l[4] = sprintf("%2d",$l[4]);
      $l[5] = sprintf("%2d",$l[5]);
      $l[6] = sprintf("%2d",$l[6]);
      $l[7] = sprintf("%2d",$l[7]);
      $l[8] = sprintf("%2d",$l[8]);
      $l[9] = sprintf("%2d",$l[9]);
      $l[10]= sprintf("%2d",$l[10]);
      print O1 "PP @l\n";
    }
    my $fit_pol = 1 - $c/$C;
    my $vec_fit_pure = sqrt($fit_pol**2 + $var_psp**2); 
    my $vec_fit_rela = sqrt(100/$WPol*($fit_pol)**2 + 100/$WPwp*($var_psp**2)); 

    # format
    my $NN        = sprintf("%6d",$i);
    $fit_pol      = sprintf("%8.3f",$fit_pol);
    $vec_fit_pure = sprintf("%8.3f",$vec_fit_pure);
    $vec_fit_rela = sprintf("%8.3f",$vec_fit_rela);
    $var_psp      = sprintf("%8.3f",$var_psp);
    $std_psp      = sprintf("%8.3f",$std_psp);
    print O1 "OO $NN $c $C $fit_pol $var_psp $std_psp   --- $vec_fit_pure $vec_fit_rela\n";
    print O2 " $NN $fit_pol $var_psp $std_psp   --- $vec_fit_pure $vec_fit_rela\n"; 
 }


 #################################################################
 #################################################################

 sub DefineOutFiles {

     my $in = shift(@_);
     my $o1; 
     my $o2;

     my @s = split(/\./,$in);
     my $o1= $s[0]."P.out";   
     my $o2= $s[0]."V.out";   

     return ($o1,$o2);

 }

 #################################################################

 sub GetAllPoll {

     my @in = @_;
     my @ou;
     for(my $i=0; $i<=$#in; $i++) {
        my @l = split(/([\/_])/,$in[$i]);
        push(@ou,$l[$#l-2].".pol") if("$l[$#l-0]" eq "$z");
     }
     return @ou;
 }

 sub GetAllDirs2 {

     my $dic = shift(@_);
     my $z   = shift(@_);
     my @dir = @_;
     my @all = ();

     foreach $_ (@dir) {
        chomp($_);
        my @qq = split(/\_/,$_);
        next if("$_" eq ".");
        next if("$_" eq "..");
        next if("$_" eq ".DS_Store");
        next if("$qq[$#qq]" eq "cp");
        next if("$qq[$#qq]" eq "sp");
        next if("$qq[$#qq]" eq "var");
        next if("$qq[$#qq]" ne "$z");
        push(@all,"$wrkp/$z/$_");
     }

     return @all;


 }

 sub GetAllDirs {

     my $dic = shift(@_);
     my $z   = shift(@_);
     my @dir = @_;
     my @all = ();


     foreach $_ (@dir) {
        chomp($_);
        my @qq = split(/\_/,$_);

#       if("$_" eq "$z") {
#       my @tmp = split('',$_);
#       next if($#tmp!=$dic);
#       if("$tmp[0]" eq "0" && "$tmp[1]" eq "0" && $tmp[2] == $dept) {
#       opendir(SBD,"$wrkp/$_"); my @sbd=readdir(SBD); close(SBD);


        if("$qq[1]" eq "$z") {
        opendir(SBD,"$wrkp/$z"); my @sbd=readdir(SBD); close(SBD);
#       print "@sbd\n";
        
#       exit;
        for(my $j=0; $j<=$#sbd; $j++) {
           print "ciao $wrkp/$z/$_ $sbd[$j]\n";
           next if("$sbd[$j]" eq ".");
           next if("$sbd[$j]" eq "..");
           next if("$sbd[$j]" eq ".DS_Store");
           my @f = split(/\_/,$sbd[$j]);
           next if("$f[$#f]" eq "sp");
           next if("$f[$#f]" eq "co");
           next if("$f[$#f]" eq "var");
           next if("$f[$#f-0]" ne "$z");

           print "-------------- $wrkp/$_/$sbd[$j]\n";
#          push(@all,"$wrkp/$_/$sbd[$j]");
       }
    }
 }

  print "zzzzzzzzzzzzz @all\n";
  return (@all);
  }
