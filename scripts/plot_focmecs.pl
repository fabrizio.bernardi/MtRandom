#!/usr/bin/perl

 ##################################################################################
 #                                                                                #
 # Plot mt-random solution and power spectra fit                                  #
 #                                                                                #
 ##################################################################################

 use Env;
 use strict;
 use Math::FFT;
 use Math::VecStat qw(max min maxabs minabs sum average);
 use Math::Complex;
 use File::Path;
 use Array::Unique;
 use Statistics::Basic qw(:all);
 use Math::Combinatorics;
 use Algorithm::Permute;

 (my $yyyy, my $dept, my $oblib, my $outfile, my $tops, my $row, my $spin, my $id, my @dim)  = @ARGV;

 if($#ARGV != 12) {
   print "Usage: $0 year depth observed_file.inn outfile NrMts NrRow P-fit_spin $id dim\n";
   print "       $0 1917 5 1917_0426A.inn 1917_0426AV.out 50 3 0.1 test 0 180 1.5 3 60\n";
   exit;
 }
 my $z   = sprintf("%03s",$dept);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200
 my $xm=$dim[0];
 my $xM=$dim[1];
 my $ym=$dim[2];
 my $yM=$dim[3];
 my $PM=$dim[4];
 my $xs=9;
 my $ys=9;

 ##############################################################################
 # Paths
 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $wrkd  = "/Users/fabrizio/MTs/work/$yyyy/$z";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $mtp   = "/Users/fabrizio/MTs/lib/mtlib_3.1_05000.lib";
 my $libre = "mtlib_3";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";
 my $obsf  = "/Users/fabrizio/MTs/work/$yyyy/observed";

 ##################################################################
 #########  Begin  Preparation data_plot

 ##################################################################
 # Open and Read staz lib, and MT-work, and observed file, observed path
 open(OBS,"<$wrkp/$oblib");   my @obs=<OBS>; close(OBS);
 open(RIT,"<$wrkd/$outfile"); my @rit=<RIT>; close(RIT);
 open(MTP,"<$mtp");           my @mtp=<MTP>; close(MTP);
 opendir(DIR,"$wrkp"); my @dir=readdir(DIR); close(DIR);
 opendir(ODI,"$obsf"); my @odi=readdir(ODI); close(ODI);
 
 ##################################################################
 # Find Best $tops NrMt following $row criteria
 my @sols = SelectSols($row, $spin);

 ##################################################################
 # find focal parameters for each @sols
 my @mts = FindSourcePars1(@sols);
 my @axe = FindSourcePars2(@sols);

 ##################################################################
 # Name outputfiles 
 my @outna = GetOutNames($oblib,$id,$z);

 ##################################################################
 ######### Begin generation of gmt script
 ##################################################################

 

  my $gmtp   = "$wrkp/plots";
  my $gmtstr = "gmtset ANNOT_FONT_SIZE         = 6p \n";
     $gmtstr.= "gmtset LABEL_FONT_SIZE         = 10p\n";
     $gmtstr.= "gmtset LABEL_OFFSET            = 0.001i\n";
     $gmtstr.= "gmtset HEADER_FONT_SIZE        = 7p\n";
     $gmtstr.= "gmtset HEADER_OFFSET           = 0.081i\n";
     $gmtstr.= "gmtset CHAR_ENCODING           = ISOLatin1+\n";

  ### Write header, with earthquake info and main parameters solution
  # plot solution from last of the selected to the top
  my $G;


  my $c=0;
  for(my $i=0; $i<=$tops; $i++) {
     my $var = $sols[$tops-$i];
     my $pla = $mts[$tops-$i];
     my $plu = $axe[$tops-$i];
     my @va=split(' ',$var);
     my @pl=split(/\_/,$pla);
     my @lu=split(/\_/,$plu);

     next if($va[$row-1]>$yM);
 
     my $xP = $lu[2];
#       $xP = $pl[1]  if($pl[2]>=$pl[1]);
#    print "$va[0] $va[$row-1] * @pl\n"; 
     if($va[1]<=0) {$G = "-G0/0/250"}
     else          {$G = "-G100/100/100"}
     if($c==0) {
       $gmtstr .= "psmeca << EOF -R$xm/$xM/$ym/$yM -JX$xs/$ys $G -L -K -Sc2.5 -N -C ";
       $gmtstr .= "-X3 -Y20 -Ba10f2:\"Azimuth T-axe\":/a0.2f.05:\"Var\":WS > $gmtp/$outna[0]\n";
       $gmtstr .= "$xP $va[$row-1] 0 $pl[1] $pl[2] $pl[3] $pl[4] $pl[5] $pl[6] 1. 0 0 0\n";
       $gmtstr .= "EOF\n";

     }
     elsif($c>0 && $i<$tops) {
       $gmtstr .= "psmeca << EOF -R$xm/$xM/$ym/$yM -JX$xs/$ys $G -L -K -O ";
       $gmtstr .= "-Sc2.5 -N -C >> $gmtp/$outna[0]\n";
       $gmtstr .= "$xP $va[$row-1] 0 $pl[1] $pl[2] $pl[3] $pl[4] $pl[5] $pl[6] 1. 0 0 0\n";
       $gmtstr .= "EOF\n";
     }
     else{
       $gmtstr .= "psmeca << EOF -R$xm/$xM/$ym/$yM -JX$xs/$ys -G250/0/0 -L -O -K ";
       $gmtstr .= "-Sc4.5 -N -C -B::/::ne >> $gmtp/$outna[0]\n";
       $gmtstr .= "$xP $va[$row-1] 0 $pl[1] $pl[2] $pl[3] $pl[4] $pl[5] $pl[6] 1. 0 0 0\n";
       $gmtstr .= "EOF\n";
     }
     $c++; 

  }

  $c=0;
  $ym=0;
  $yM=90;
  my @pll=();
  for(my $i=0; $i<=$tops; $i++) {
     my $var = $sols[$tops-$i];
     my $pla = $mts[$tops-$i];
     my $plu = $axe[$tops-$i];
     my @va=split(' ',$var);
     my @pl=split(/\_/,$pla);
     my @lu=split(/\_/,$plu);
     my $emp = join("_",$lu[1],$lu[2]);
     push(@pll,$emp);

     next if($va[$row-1]>$yM);
 
     my $xP = $lu[1];
#       $xP = $pl[1]  if($pl[2]>=$pl[1]);
#    print "$va[0] $va[$row-1] * @pl\n"; 
     if($va[1]<=0) {$G = "-G0/0/250"}
     else          {$G = "-G100/100/100"}
     if($c==0) {
       $gmtstr .= "psmeca << EOF -R$xm/$xM/$ym/$PM -JX$xs/$ys $G -L -K -O -Sc2.5 -N -C ";
       $gmtstr .= "-Y-11 -Ba10f2:\"Azimuth T-axe\":/a10f2:\"Plg T-Axe\":WS >> $gmtp/$outna[0]\n";
       $gmtstr .= "$lu[2] $lu[1] 0 $pl[1] $pl[2] $pl[3] $pl[4] $pl[5] $pl[6] 1. 0 0 0\n";
       $gmtstr .= "EOF\n";

     }
     elsif($c>0 && $i<$tops) {
       $gmtstr .= "psmeca << EOF -R$xm/$xM/$ym/$PM -JX$xs/$ys $G -L -K -O ";
       $gmtstr .= "-Sc2.5 -N -C >> $gmtp/$outna[0]\n";
       $gmtstr .= "$lu[2] $lu[1] 0 $pl[1] $pl[2] $pl[3] $pl[4] $pl[5] $pl[6] 1. 0 0 0\n";
       $gmtstr .= "EOF\n";
     }
     else{
       $gmtstr .= "psmeca << EOF -R$xm/$xM/$ym/$PM -JX$xs/$ys -G250/0/0 -L -O ";
       $gmtstr .= "-Sc4.5 -N -C -B::/::ne >> $gmtp/$outna[0]\n";
       $gmtstr .= "$lu[2] $lu[1] 0 $pl[1] $pl[2] $pl[3] $pl[4] $pl[5] $pl[6] 1. 0 0 0\n";
       $gmtstr .= "EOF\n";
     }
     $c++; 

  }

  ##############################################################################
  # Tries to find families
  my $analisi = 1;
  if($analisi==1) {
  my @data=@pll;
  my @sets=();
  my $nr = 3;
  my $n  = sprintf("%d",($#data+1)/3);
  my $i=0;
  my $p = new Algorithm::Permute([@data], $n);
  my @rus=();
  while (my @res = $p->next) {
    

    my $d = Math::Combinatorics->new(count => 2,
                                     data => [@res],
                                    );

    my $spin=check($#rus,$#res,@rus,@res);
    if($spin == 0) {
      @rus=@res;
      $i++;
      next;
    }

    my @delta1=();
    while (my @dat = $d->next_combination) {

        my @e1 = split(/\_/,$dat[0]);
        my @e2 = split(/\_/,$dat[1]);
        my $dist = GetDist(@e1,@e2);
        push(@delta1,$dist);
#       print join(", ", @dat), ," ",$dist , @e1 , @e2, "\n";
    }
    my $mean=sprintf("%.3f",mean(@delta1));
    my $stde=sprintf("%.3f",stddev(@delta1));
    $sets[$i] = join(", ", @res,$mean,$stde);
    print ">>>>>>>>>>   ", join(", ", @res), "  ", $mean, "  ", $stde,"\n";
#   print "$mean\n";
    @rus=@res;
    $i++;
    
 }

 }


  system("$gmtstr");
  system("ps2pdf $gmtp/$outna[0] $gmtp/$outna[1]");
  system("open $gmtp/$outna[1]");



 ##################################################################
 ##################################################################


 sub check {

     my $O = shift(@_);
     my $N = shift(@_);
     my @o;
     my @n;
     my $ou = 1;

     if($0!=-1) {
      for(my $k=0; $k<=$O; $k++) {
        @o[$k] = shift(@_);
      } 
      @n = @_;
      my @so = sort{$a <=> $b} @o;
      my @sn = sort{$a <=> $b} @n;
      my $lo = join("_",@so);
      my $ln = join("_",@sn);
      $ou = 0 if("$lo" eq "$ln")
     }

     return $ou



 }


 sub GetOutNames {

     my $root = shift(@_);
     my $id   = shift(@_);
     my $z    = shift(@_);
     my @ou;
     my @l =split(/A/,$root);
     $ou[0]="MechsFam.$l[0].$z.$id.ps";
     $ou[1]="MechsFam.$l[0].$z.$id.pdf";
     return @ou;

 }

 sub FindSourcePars2 {

     my @ins = @_;
     my @pla;
     my @out;
 
     for(my $i=0; $i<=$#ins; $i++) {
         chomp($ins[$i]);
         my @l = split(' ',$ins[$i]);
         my $np1 = $mtp[$l[0]*10+7];
         my $np2 = $mtp[$l[0]*10+8];
         my $np3 = $mtp[$l[0]*10+9];
         chomp($np1);
         chomp($np2);
         chomp($np3);
         my @p1 = split(' ',$np1);
         my @p2 = split(' ',$np2);
         my @p3 = split(' ',$np3);
         $pla[0] = $p1[4];
         $pla[1] = $p1[5];
         $pla[2] = $p1[3];
         $pla[3] = $p2[4];
         $pla[4] = $p2[3];
         $pla[5] = $p2[4];
         my $solution = join('_',$l[0],@pla);
         $out[$i] = $solution;
     }
     return @out;

 }


 sub FindSourcePars1 {

     my @ins = @_;
     my @pla;
     my @out;
 
     for(my $i=0; $i<=$#ins; $i++) {
         chomp($ins[$i]);
         my @l = split(' ',$ins[$i]);
         my $np1 = $mtp[$l[0]*10+5];
         my $np2 = $mtp[$l[0]*10+6];
         chomp($np1);
         chomp($np2);
         my @p1 = split(' ',$np1);
         my @p2 = split(' ',$np2);
         $pla[0] = $p1[3];
         $pla[1] = $p1[4];
         $pla[2] = $p1[5];
         $pla[3] = $p2[2];
         $pla[4] = $p2[3];
         $pla[5] = $p2[4];
         my $solution = join('_',$l[0],@pla);
         $out[$i] = $solution;
     }
     return @out;

 }


 sub SelectSols {

     (my $row, my $spi) = @_;
     my @lin;

     # fist select solution taht satisfy P-polarity criteria
     for(my $i=0; $i<=$#rit; $i++) {
         chomp($rit[$i]);
         my @l = split(' ',$rit[$i]);
         push(@lin, $rit[$i]) if($l[1] <= $spi);
     }    

     my @sorted = map {$_->[0]}
        sort { $a->[$row] <=> $b->[$row]} 
        map {chomp;[$_,split(' ')]} @lin;

     return @sorted;

 }

 sub GetDist {

  my $lat_a = shift(@_);
  my $lon_a = shift(@_);
  my $lat_b = shift(@_);
  my $lon_b = shift(@_);

  my $d2r =  0.017453293;    #degree-to-radians
  my $r2d = 57.295779515;    #radians-to-degree
  my $deg2km = 1 * $d2r;  #degree-to-kilometers
                          #this assumes the average Earth radius to be 6371 km

  #use geocentric latitude instead of geographic latitude
  #to calculate distance-aximuth-backazimuth

  #eccentricity e2:
  my $e2 = .0;      #Geodetic Reference System'80 GRS-80

  #check input
  if ($lat_a > 90 || $lat_a < -90 || $lat_b > 90 || $lat_b < -90)
     {
     print "Latitude has to be in the -90 to +90 degree range!\n";
     exit;
     }
  while ($lon_a > 360) {$lon_a -= 360;}
  while ($lon_b > 360) {$lon_b -= 360;}
  while ($lon_a < 0) {$lon_a += 360;}
  while ($lon_b < 0) {$lon_b += 360;}

  #convert from geograpic to geocentric latitude:
  my $lat_a_r = $lat_a * $d2r;
  my $lat_b_r = $lat_b * $d2r;
  my $lat_a_r_geoc = atan2 ( (1-$e2)*sin($lat_a_r)/cos($lat_a_r),1 );
  my $lat_b_r_geoc = atan2 ( (1-$e2)*sin($lat_b_r)/cos($lat_b_r),1 );
  my $lat_a = $lat_a_r_geoc * $r2d;  #now geocentric latitude!
  my $lat_b = $lat_b_r_geoc * $r2d;  #now geocentric latitude!

  #colatitude = 90 - latitude!
  my $colat_a = 90 - $lat_a;
  my $colat_b = 90 - $lat_b;

  my $colat_a_r = $colat_a * $d2r;
  my $lon_a_r = $lon_a * $d2r;
  my $colat_b_r = $colat_b * $d2r;
  my $lon_b_r = $lon_b * $d2r;
  #
  my $tmp = cos($colat_a_r)*cos($colat_b_r) + sin($colat_a_r)*sin($colat_b_r)*cos($lon_b_r-$lon_a_r);
  my $distance_r = ACOS($tmp);
  my $distance = $distance_r * $r2d;
  my $distance_km = $distance * $deg2km;


  return($distance);

 }

 sub ACOS { my $acos_rad = atan2( sqrt(1 - $_[0] * $_[0]), $_[0] );
            return $acos_rad;
          }

 sub ASIN { my $asin_ang = atan2($_[0], sqrt(1 - $_[0] * $_[0]) );
            return $asin_ang;
          }


