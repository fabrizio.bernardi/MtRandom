#!/usr/bin/perl

 ##########################################################################################
 # 											  #
 # Make conbvolution between stations and MT and Green for big_libs                       #
 # Updates                                                                                #
 # - Put comvoluted seismograms into: /Users/fabrizio/MTs/work/YYYY and not in            #
 #                                    /Users/fabrizio/MTs/work                            #
 #   This allow the run of multiple events                                                # 
 # 											  #
 ##########################################################################################

 use strict;
 use Env;
 use File::chdir;
 use File::Path;

 ##########################################################################################
 # SEt pth and variables
 if($#ARGV != 2) {
    print "Usage: $0 depth station_file_inn year\n";
    print "       $0 5 1917_station_file.inn [/Users/fabrizio/lib/staz_mt_random] 1917\n";
    exit; 
 }
 my $z     = $ARGV[0];
 my $satzF = $ARGV[1];
 my $subPY = $ARGV[2];
 my $workp = "/Users/fabrizio/MTs/work";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $stazp = "/Users/fabrizio/lib/staz_mt_random";
 my $mtpat = "/Users/fabrizio/MTs/lib";
 my $mtlib = "mtlib_3.1_05000.lib";
#my $mtlib = "mtlib_3.1_00000.lib";
#my $mtlib = "mtlib_2.0_10000.lib";
 my $Run_Synt = "run_Synt_forMT.pl";
 my @dwidt = (200,2300);
 my $z     = sprintf("%03d",$z);
#my $PP    = $z.$su;
 
 ##########################################################################################
 # Station file and MT libs
 open(STA,"<$stazp/$satzF"); my @sta=<STA>; close(STA);
#opendir(DIR,"$mtslp"); my @dir=readdir(DIR); close(DIR);
 my @coo_e = GetCoordinatesEvent($sta[0],$z);
 shift(@sta);
  
 ##########################################################################################
 # Set how many lib want to use
#my $minlib=0;
#my $maxlib=1;
#my @mtlibs=selectMTlibs($minlib,$maxlib,@dir);
#print "@mtlibs\n";
 
 ##########################################################################################
 # Begin loopes
 # 1: MT lib
 # 2: Stazs
#for(my $j=0; $j<=$#mtlibs; $j++) {
#for(my $j=0; $j<=1; $j++) {

     for(my $k=0; $k<=$#sta; $k++) {
#    for(my $k=0; $k<=1; $k++) {

       chomp($sta[$k]);
       # Leggere info
       # fare dir
       # entrare dir
       # crivere staz file
       # convoluzione
       (my $ST, my $POL, my $la, my $lo, my $liv, my $de, my $az, my $bz, my $km) = split(' ',$sta[$k]);
       next if($POL==0);
       next if($km < $dwidt[0] || $km > $dwidt[1]);
       my $st_d = $ST."_".$z;
#      mkpath( "$workp/$subPY/$z/$st_d",  {verbose =>0}); 
       system("mkdir -p $workp/$subPY/$z/$st_d"); 
       print "$workp/$subPY/$z/$st_d\n";
       chdir("$workp/$subPY/$z/$st_d");
       my $aa = $ST."_staz";
       open(SF,">$workp/$subPY/$z/$st_d/$aa");
           print SF "EVENT @coo_e\n";
           print SF "$ST $la $lo 0"; 
       close(SF); 
       print "$binp/$Run_Synt $ST $km $az $coo_e[2] $mtlib $workp/$subPY/$z/$st_d $workp/$subPY/$z/$st_d v $aa $subPY \n";
       system("$binp/$Run_Synt $ST $km $az $coo_e[2] $mtlib $workp/$subPY/$z/$st_d $workp/$subPY/$z/$st_d v $aa $subPY"); 

    }

#}




 ##########################################################################################
 ##########################################################################################

 sub GetCoordinatesEvent {

    my $e = shift(@_);
    chomp($e);
    my $z = shift(@_);
    my @l = split(' ',$e);
    return ($l[1],$l[2],$z);

 }

 ##########################################################################################

 sub GETNRLIB {

    my @foe = split(/([._D])/,$_[0]);
    my $nr = sprintf("%d",$foe[$#foe]);
    return $nr;

 }


###########################################################################################

#sub selectMTlibs {

#   my @out = ();
#   my $m = shift(@_);
#   my $M = shift(@_);
#   my @in = @_; 
#   

#   for(my $j=0; $j<=$#in; $j++) {

#      chomp($in[$j]);
#      next if("$in[$j]" eq ".");
#      next if("$in[$j]" eq "..");

#      my @tt = split(/\./,$in[$j]);
#      next if("$tt[$#tt]" ne "lib");
#      my $nr = GETNRLIB($tt[$#tt-1]);
#      push(@out,$in[$j]) if($nr >= $m && $nr <= $M);

#   }
#   return @out;
#
#}



#cd /Users/fabrizio/MTs/work/GTT_22
#cp /Users/fabrizio/MTs/work/GTT_04/GTT_StazFile .
#../../bin/Run_Synt.pl GTT  893 350.6 22 mt_lib_n10000_ID0001.lib . . v GTT_StazFile
#exit
