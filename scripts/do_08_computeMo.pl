#!/usr/bin/perl

 ##############################################################################
 #                                                                            #
 # Compute instrument response and make convolution with syntetics (vi sac)   #
 #                                                                            #
 ##############################################################################

 # Packages
 use strict;
 use Env;
 use Env;
 use strict;
 use Math::FFT;
 use Math::VecStat qw(max min maxabs minabs sum average);
 use Math::Complex;
 use File::Path qw(make_path remove_tree);
 use Array::Unique;
 use Statistics::Basic qw(:all);

 ##############################################################################
 # Arguments
 (my $yyyy, my $oblib, my $stlib, my $dept, my $MTnr) = @ARGV;
 if($#ARGV != 4) {
   print "Usage: $0 year observed_file.inn response.inn depth NR_mt\n";
   print "       $0 1917 1917_0426A.inn 1917_response_file.inn 5 2267\n";
   exit;
 }
 my $z = sprintf("%03s",$dept);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200 
 my $Mok = 1e25; 

 ##############################################################################
 # Paths
 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $mtp   = "/Users/fabrizio/MTs/lib";
 my $libre = "/Users/fabrizio/lib/polezero";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";
 make_path("$wrkp/$z/Mo");

 ##############################################################################
 # constants for fft
 my $NmaxPoints = 1024*2;;
 my $tpi = 2.*acos(-1.0);
 my $tpi = 1;
 my $pi = 4 * atan2(1, 1);
 my $nor = 1;
 my $om;
 my @lin;
 my $T;

 ##################################################################
 # Open and Read staz lib, and MT-work
 open(OBS,"<$stp/$oblib"); my @obs=<OBS>; close(OBS);
 open(STA,"<$stp/$stlib"); my @sta=<STA>; close(STA);
 opendir(DIR,"$wrkp/observed"); my @dir=readdir(DIR); close(DIR);

 ##################################################################
 # Select stations and comp (synt) to convolve from observed_file.inn
 # should take also *pz files
 # synt
 my @syntfiles = SelectSyntFiles($wrkp,$z,$MTnr,@obs); 
 # obs
 my @obsfiles = SelectObsFiles($wrkp,$z,$MTnr,@obs);
 for(my $i=0; $i<=$#obsfiles; $i++) {
#print ">>> $syntfiles[$i]  $obsfiles[$i]\n";
 }

 ##################################################################
 # convolve synt with instrument response
 (my $string, my @syntfiles) = ConvolveSynt(@syntfiles); 
 # apply convolution
 system("gsac <<EOj &>/dev/null
         $string");


 ##################################################################
 # @synt and @obs are already syncronized
 # Apply for each couple fft, compute power spectrum and take max 
 # Mo,i=Max(syn)/Max(obs) * Mo,k 
 my @MaxObs;
 my @MaxSyn;
 my @Mo;
 for(my $i=0; $i<=$#syntfiles; $i++) {

    # Read Sac
    (my $dts,my @syn) = ReadSac("$wrkp/$z/Mo/",$syntfiles[$i]);
    (my $dtd,my @obs) = ReadSac("$wrkp/observed/",$obsfiles[$i]);
#   print "$syntfiles[$i] $obsfiles[$i]\n";
     
    # Make fft
    my $syn = \@syn;
    my $syn_fft   = new Math::FFT($syn);
    my $syn_spctr = $syn_fft->spctrm;
    my $obs = \@obs;
    my $obs_fft   = new Math::FFT($obs);
    my $obs_spctr = $obs_fft->spctrm;

    my $dom   = 1/($NmaxPoints*$dtd);

    @lin = ();
#   print "\n\n>> $syntfiles[$i]\n";
    my @info= split(/\./,$syntfiles[$i]);
    my $station = sprintf("%-5s",$info[0]);
    my $compone = sprintf("%1s",$info[$#info-1]);
    my $sss=0;
    
#      for(my $i = 1; $i<$#syn/2; $i++) {
       for(my $i = 20; $i<200; $i++) {
          my $a = $syn_spctr->[$i];
          my $b = $obs_spctr->[$i];
          $lin[$i-1] = ($a-$b);       
          $sss+=($a-$b);
#         $syn_spctr->[$i] = $syn_spctr->[$i]*$nor/$max;
          $om = $dom * $i * $tpi;
          $T  = 1/$om;
          my $a = sprintf("%5d  %e  %e  %e  %e",$i,$om,$T,$syn_spctr->[$i],$obs_spctr->[$i]);
#         print "$a\n";
          push(@lin,$a);
       }


#   my $aa= mean(@lin);
    # Find Max
    $MaxSyn[$i]=sprintf("%.4e",max($syn_spctr));    
    $MaxObs[$i]=sprintf("%.4e",max($obs_spctr));    

    # Mo
    $Mo[$i]=sprintf("%.4e",($MaxObs[$i]/$MaxSyn[$i]));

    print "$station $compone -> $Mo[$i] $MaxSyn[$i] $MaxObs[$i]\n";

 }

 for(my $k=0; $k<=$#Mo; $k++) {
    $Mo[$k] *= $Mok;
 }

 print "------- @Mo\n";
 
 ##################################################################
 # Evaluate Mo and Mw
 my $MeaMo = sprintf("%.4e",mean(@Mo));
 my $MinMo = sprintf("%.4e",min(@Mo));
 my $MaxMo = sprintf("%.4e",max(@Mo));
 my $StdMo = sprintf("%.4e",stddev(@Mo));
 my $MedMo = sprintf("%.4e",median(@Mo));
 @Mo = sort {$a <=> $b} @Mo;
 print "@Mo\n";
 shift(@Mo);
 @Mo = sort {$b <=> $a} @Mo;
 shift(@Mo);
 print "@Mo\n";
 my $MeanPMo = sprintf("%.4e",mean(@Mo));
 my $StdPMo = sprintf("%.4e",stddev(@Mo));
 print "\n";
 print "Min:    $MinMo \n";
 print "Max:    $MaxMo \n";
 print "Mean:   $MeaMo \n";
 print "Std:    $StdMo \n";
 print "MeanP:  $MeanPMo\n";
 print "StdP:   $StdPMo\n";
 print "Median: $MedMo\n";
 

 sub ConvolveSynt {

     my @in = @_;
     my $str;
     my @ou;
     for(my $i=0; $i<=$#in; $i++) {
         my @ob = split(/-/,$in[$i]);
         my @f = split(/\//,$ob[0]);
         my $o = $f[1].".co"; 
         push(@ou,$o);
         $str .= "r $wrkp/$z/$ob[0]\n";
         $str .= "transfer from none to polezero subtype $wrkp/$z/$ob[1]\n";
         $str .= "w $wrkp/$z/Mo/$o\n";
      }
      return ($str,@ou);

 }


 sub SelectObsFiles {


     my $pt = shift(@_);
     my $z  = shift(@_);
     my $NR = shift(@_);
     my @in = @_;
     my @ou;
     shift(@in); shift(@in);
     my @t = split(/A/,$oblib);
     for(my $i=0; $i<=$#in; $i++) {
        chomp($in[$i]);
        my @f = split(' ',$in[$i]);
        next if($f[2]==0);  #next if station is off for spectra analysis
        if($f[3]==1) {
          my $a = join('.',$f[0],$f[1],$t[0],"z","sac");
          push(@ou,$a);
        }
        if($f[4]==1) {
          my $a = join('.',$f[0],$f[1],$t[0],"n","sac");
          push(@ou,$a);
        }
        if($f[5]==1) {
          my $a = join('.',$f[0],$f[1],$t[0],"e","sac");
          push(@ou,$a);
        }
     }

     return @ou;
 }

 sub SelectSyntFiles {

     my $pt = shift(@_);
     my $z  = shift(@_);
     my $NR = shift(@_);
     my @in = @_;
     my @ou;
     shift(@in); shift(@in);
     $NR = sprintf("%05d",$NR);
     for(my $i=0; $i<=$#in; $i++) {
        chomp($in[$i]);
        my @f = split(' ',$in[$i]);
        next if($f[2]==0);  #next if station is off for spectra analysis
        my $synt1="X"; my $synt2="X"; my $synt3="X";
        my $resp1="X"; my $resp2="X"; my $resp3="X";
        my $join1="X"; my $join2="X"; my $join3="X";
        my $pp   = $f[0]."_".$z;
        my $pc   = $f[0]."_".$z."_co";
        if($f[3]==1) {
          $synt1 = $f[0].".mtlib_3.".$NR.".".$z.".z";
          $resp1 = $f[0].".z.pz";
          $join1 = $pp."/".$synt1."-".$pc."/".$resp1;  
        push(@ou,$join1); 
        }
        if($f[4]==1) {
          $synt2 = $f[0].".mtlib_3.".$NR.".".$z.".n";
          $resp2 = $f[0].".n.pz";
          $join2 = $pp."/".$synt2."-".$pc."/".$resp2;
        push(@ou,$join2); 
        }
        if($f[5]==1) {
          $synt3 = $f[0].".mtlib_3.".$NR.".".$z.".e";
          $resp3 = $f[0].".e.pz";
          $join3 = $pp."/".$synt3."-".$pc."/".$resp3;
        push(@ou,$join3); 
        }
        #push(@ou,$join1,$join2,$join3); 
        
        
     }

     return @ou;
 }

sub ReadSac {

     my $pa = shift(@_);
     my $st = shift(@_);
     my @ou = ();
     open(FH,"<$pa/$st") or die; binmode(FH);
     my $npts  = ReadValue(79,4,'i');
     my $delta = ReadValue(0,4,'f');
     if($npts<$NmaxPoints) {
        for(my $i=158; $i<=158+$npts; $i++) {
           my $a=ReadValue($i,4,'f');
           push(@ou,$a);
        }
        for(my $i=0; $i<=$NmaxPoints-$npts-2; $i++) {
           push(@ou,0);
        }
     }
     if($npts>=$NmaxPoints) {
        for(my $i=158; $i<=158+$NmaxPoints-1; $i++) {
           my $a=ReadValue($i,4,'f');
           push(@ou,$a);
        }

     }
     return ($delta,@ou);

 }

 sub ReadValue {

   seek(FH, @_[0]*@_[1],0);
   read(FH, my $tmp, @_[1]);
   $tmp=unpack(@_[2], $tmp);
   return $tmp;

 }

