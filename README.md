# istallazione


# Passi di base per l'esecuzione


1) Creare directory di lavoro per ogni lavoro (può essere ovunque)
   
    mkdir ~/eventi/1920_0906_Garfagnana
    mkdir ~/eventi/1920_0906_Garfagnana/infiles
    cd ~/eventi/1920_0906_Garfagnana
    
2) Creare input files (stazioni, polarità, filtri, risposta) nella sottodirectory infiles (vedi formato)

    cd  ~/eventi/1920_0906_Garfagnana/infiles
    touch 1920_0906A.inn
    touch 1920_0906_response_file.inn
    touch 1920_0906_station_file.inn
   
   

# Formato dei files di input

#### YYYY_MMDDA.inn

esempio:

    1930 10 30 00 00 00.0  0 005 025 50 50
       43.659  13.331   8.0  0.0  0.0 
    ALI   W   0  0  0  0  0  0  0  0 005 020 005 020 005 020  0  0  0  0
    ATH   W   1  0  1  0  0  0  0  0 005 020 005 020 005 020  1  1  0 -1
    CHE   W   1  0  0  1  0  0  0  0 005 020 005 020 005 020  1  0  0 -1
    COP   G   1  0  0  1  0  0  0  0 005 020 005 020 005 020 -1  0  0  0

##### Riga 1 
 ```   
YYYY MM DD hh mm ss.s spin1 T_all_min T_all_max int int
YYYY    --> anno
MM      --> mese
DD      --> giorno
hh      --> ora
mm      --> minuto
ss.s    --> secondi
spin1   --> [1=filtro T_all_min T_all_max per tutte le stazioni acceso / 0=filtro T_all_min T_all_max per tutte le spento]
int int --> 50 50 (non ricordo il perché)
```

##### Riga 2
```
lat lon depth float float
```

##### Righe 3->N
```
CODE INSTR_TYPE spin_insrument spin_EW spin_NS spin_Z ph_EW ph_NS ph_Z int tmin t_max (E N Z) polarity_station polarity (ENZ)

INSTR_TYPE G: Galitzin type      W: Wiechert Type
```

#### YYYY_station_file.inn

esempio:
   
     EVENT  43.643 13.260  10.5    Sinigaglia
     ALI    38.355  -0.487    0.0000  
     ALM    36.853  -2.460    0.0000
     
##### Riga 1 
 ```
 EVENT lat lon depth Area
 ````

##### Righe 2->N
```
CODE lat lon elevation
```
    