#!/usr/bin/perl

 ###################################################################################
 #                                                                                 #
 # Rotate syntetics for those storic stations with non conventional orientation    #
 #                                                                                 #
 # Convention: NS  --> SE-NW:+135  --> NW-SE:-45                                   #
 #             EW  --> SW-NE:+135  --> NE-SW:-45                                   #
 #                                                                                 #
 ###################################################################################

 use strict;
 use Env;

 # Arguments
 (my $yyyy, my $oblib, my $rotf, my $dept) = @ARGV;
 if($#ARGV != 3) {
   print "Usage: $0 year observed_file.inn rotation.inn depth\n";
   print "       $0 1917 1917_0426A.inn 1917_rotatio_file.inn 5\n";
   exit;
 }
 my $z = sprintf("%03s",$dept);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200


 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $mtp   = "/Users/fabrizio/MTs/lib";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";
 my $wrkd  = "/Users/fabrizio/MTs/work/$yyyy/$z";
 my $path;
 
 ##################################################################
 # Open and Read staz lib, and MT-work, and observed file, observed path
#open(OBS,"<$stp/$oblib"); my @obs=<OBS>; close(OBS);
#open(ROT,"<$stp/$rotf"); my @rot=<ROT>; close(ROT);
 open(OBS,"<$oblib"); my @obs=<OBS>; close(OBS);
 open(ROT,"<$rotf"); my @rot=<ROT>; close(ROT);

 ##################################################################
 # read from obs_file station to be rotated, spin[9] = 1
 my @rotation_stations=getRotationStaz(@obs);

 
 ##################################################################
 # associate the rotation to each station
 for(my $i=0; $i<=$#rotation_stations; $i++) {
     $rotation_stations[$i]=GetRotationAngle($rotation_stations[$i],@rot);
 }

 
 ##################################################################
 #apply rotation for each station
 for(my $i=0; $i<=$#rotation_stations; $i++) {

    (my $sta, my $angle) = split(/\_/,$rotation_stations[$i]);
    my $path = "$wrkp/$z/$sta"."_"."$z";

    my @files=getFiles($sta,$path);
    
    #apply rotation
    print "ROTATE $sta $angle\n";
    for(my $i=0; $i<=$#files; $i+=2) {
#       print "r $path/$files[$i+0] $path/$files[$i+1]\n";
#       print "rotate to $angle\n";
#       print "w $path/$files[$i+0] $path/$files[$i+1]\n";
        system("gsac <<EOj &>/dev/null
              r $path/$files[$i+0] $path/$files[$i+1]
              rotate to $angle
              w $path/$files[$i+0] $path/$files[$i+1]");
    }

 }


 ###################################################################################
 ###################################################################################


 sub getFiles {

   my $sta  = shift(@_);
   my $wrkp = shift(@_);
   my @ou;
   opendir(DIR,"$wrkp"); my @dir=readdir(DIR); close(DIR);
     foreach $_ (@dir) {
       chomp($_);
       next if("$_" eq ".");
       next if("$_" eq "..");
       my @f = split(/\./,$_);
       next if("$f[0]" ne "$sta");
       next if("$f[$#f]" eq "z"); 
       push(@ou,$_);

     }
    
     return @ou;

 }

 sub GetRotationAngle {

     my $sta = shift(@_);
     my @rot = @_;

     for(my $i=0; $i<=$#rot; $i++) {
         chomp($rot[$i]);
         my @f = split(' ',$rot[$i]);
         if("$sta" eq "$f[0]") {
            $sta = $sta."_".$f[1];
            last;
         }
     }
     return $sta;


 }

 sub getRotationStaz {

     my @in = @_;
     my @ou;
     for(my $i=2; $i<=$#in; $i++) {
         chomp($in[$i]);
         my @f=split(' ',$in[$i]);
         push(@ou,$f[0]) if($f[9]==1); 
     }
     return @ou;

 }


 ###################################################################################
 # 
 sub Clean {

     my @in = @_;
     my @ou = ();
     for(my $i=0; $i<=$#in; $i++) {
        chomp($in[$i]);
        my @f = split(/\./,$in[$i]);
        push(@ou,$in[$i]) if("$f[$#f]" eq "e" || "$f[$#f]" eq "n");
     } 
 
     @ou = sort {$a <=> $b} @ou;

     return @ou;
  
 }
