#!/usr/bin/perl

 ###################################################################################
 #                                                                                 #
 # Rotate station component of zagreb                                              #
 # NS --> NW-SE    : +45g  n->n                                                    #
 # NS --> NW-SE    : +45g  n->n                                                    #
 #                                                                                 #
 ###################################################################################

 use strict;
 use Env;

 (my $path) = @ARGV;
 if($#ARGV != 0) {
    print "Usage: $0 path\n";
    print "       $0 /Users/fabrizio/MTs/work/1917/005/ZAG_005\n";
    exit;
 }
  
 ###################################################################################
 # --- open path
 opendir(DIR,"$path"); my @dir=readdir(DIR); close(DIR);
 # Keep only n, e and sort
 @dir=Clean(@dir);
 # Rotate to 45 degree
 my $rotate = Rotate(45,@dir);
 # Apply rotation
 print "$rotate\n";
#system("gsac <<EOj 
#        $rotate ");

#   system("sac <<EOj &>/dev/null 
#           $merge;
#           $selec;
#   ");

 #End 



 ###################################################################################
 #
 sub Rotate {

     my $deg = shift(@_);
     my @in = @_;
     my $ou = "";
     
     for(my $i=0; $i<= $#in; $i+=2) {
        system("gsac <<EOj 
              r $path/$in[$i+0] $path/$in[$i+1]
              rotate to $deg
              w $path/$in[$i+0] $path/$in[$i+1]");
#       $ou .= "r $in[$i+0] $in[$i+1]\n"; 
#       $ou .= "rotate to $deg\n";
#       $ou .= "write $in[$i+0] $in[$i+1]\n";
     }
     for(my $i=0; $i<= $#in; $i+=2) {
        $ou .= "r $path/$in[$i+0] $path/$in[$i+1]\n"; 
        $ou .= "rotate to $deg\n";
        $ou .= "w $path/$in[$i+0] $path/$in[$i+1]\n";
        last;
     }
     return $ou;

 } 

 ###################################################################################
 # 
 sub Clean {

     my @in = @_;
     my @ou = ();
     for(my $i=0; $i<=$#in; $i++) {
        chomp($in[$i]);
        my @f = split(/\./,$in[$i]);
        push(@ou,$in[$i]) if("$f[$#f]" eq "e" || "$f[$#f]" eq "n");
     } 
 
     @ou = sort {$a <=> $b} @ou;

     return @ou;
  
 }
