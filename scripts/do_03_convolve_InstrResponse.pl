#!/usr/bin/perl

 ##############################################################################
 #                                                                            #
 # Compute instrument response and make convolution with syntetics (vi sac)   #
 #                                                                            #
 ##############################################################################

 # Packages
 use strict;
 use Env;
 use File::Path;
 use Array::Unique;

 ##############################################################################
 # Arguments
 (my $yyyy, my $stlib, my $dept) = @ARGV;
 if($#ARGV != 2) {
   print "Usage: $0 year response.inn depth\n";
   print "       $0 1917 1917_response_file.inn 5\n";
   exit;
 }
 my $z = sprintf("%03s",$dept);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200 

 ##############################################################################
 # Acessory codes
 my $make_resp = "strum2polzero";
 
 ##############################################################################
 # Paths
 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $mtp   = "/Users/fabrizio/MTs/lib";
 my $libre = "/Users/fabrizio/lib/polezero";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";

 ##################################################################
 # Open and Read staz lib, and MT-work
#open(STA,"<$stp/$stlib"); my @sta=<STA>; close(STA);
 open(STA,"<$stlib"); my @sta=<STA>; close(STA);
 opendir(DIR,"$wrkp"); my @dir=readdir(DIR); close(DIR);

 ##################################################################
 # Select Staz and paths
 my @alldirs = GetAllDirs($dic,$z,@dir);
 my @allstaz;
 tie @allstaz, 'Array::Unique';
    @allstaz = GetAllStaz($z,@dir);
#   print "@dir\n\n\n";
#   print "$wrkp \n\n\n";
#   print "@allstaz \n\n\n";
#   exit;

 ##################################################################
 # Find station which are available with instrument parameters
 # into $stp/$stlib file: costruct array with elemnt that are 
 # lines of such file
 my @Lineres = ListStazWithRespo(0,@sta); 
 my @Stazres = ListStazWithRespo(1,@sta); 
 my $temp    = join('_',@Stazres);
    @allstaz = SelectStaz(0,$temp,@allstaz);
    @alldirs = SelectStaz(1,$temp,@alldirs);

 ##################################################################
 # Make new name of dir for spectra
 my @newdirs = MakenewDirs(@alldirs);

 ##################################################################
 # Begin loop over dirs. For each dir:
 # 1. list files
 # 2. make new name
 # 3. check if newdir exist
 # 4. read response file and find which stations available
 # 5. make pole-zero file 
 # -begin loop over file
 #   6. call sac and apply convolution
 #      and save convoluted files into newdir
 # -end loop
 # End 
 
 for(my $i=0; $i<=$#alldirs; $i++) {
    
    print "$alldirs[$i]\n";
#   # 1. list files: only z,e,n files
    my @files = ReadFileList($alldirs[$i],$allstaz[$i]);

#   # 2. make new name
    my @newfi = MakeNewNameFiles(@files);

    # 3. check if newdir exist, if not, create.
    #    only for $alldirs[$i] with $i=0;
    mkpath( "$newdirs[$i]",  {verbose =>0});

#   # 4. Read response.inn file and make polezero instrument response
    # 5. make pole-zero file
    my @spin_re = MakePoleZero($newdirs[$i],$allstaz[$i],@Lineres);
#   print "$newdirs[$i] $allstaz[$i] $newfi[$i]\n";

    # 6. call sac and apply convolution  
    for(my $k=0; $k<= $#files; $k++) {
 #  for(my $k=0; $k<= 10; $k++) {
 #     
       #which polezero?
       my @f    = split(/\./,$files[$k]);
       my $pol  = $f[0].".".$f[$#f-0].".pz";

       # write convolution string
       my $str  = "r $alldirs[$i]/$files[$k]\n";
          $str .= "transfer from none to polezero subtype $newdirs[$i]/$pol\n";
          $str .= "write $newdirs[$i]/$newfi[$k]\n";

       # call sac and apply convolution 
       system("gsac <<EOj &>/dev/null
              $str");

    } #End loop over stations
 
 } #End loop over directories



 ##################################################################
 ##################################################################

 sub MakePoleZero {

     my $dire = shift(@_);
     my $staz = shift(@_);
     my @line = @_;
     my @ele  = ();
     foreach $_ (@line) {
       my @f = split(' ',$_);
       push(@ele,@f) if("$f[0]" eq "$staz");
     }

     # make poles and zeros file if spin == 1
     if("$ele[1]" eq "P") {
       if($ele[3] == 1) {
         my $out = $ele[0].".z.pz";
         system("strum2polzero $ele[1] $ele[7] $ele[8] $ele[9] > $dire/$out");
       }
       if($ele[4] == 1) {
         my $out = $ele[0].".n.pz";
         system("strum2polzero $ele[1] $ele[11] $ele[12] $ele[13] > $dire/$out");
       }
       if($ele[5] == 1) {
         my $out = $ele[0].".e.pz";
         system("strum2polzero $ele[1] $ele[15] $ele[16] $ele[17] > $dire/$out");
       }
     }
     if("$ele[1]" eq "G") {
       if($ele[3] == 1) {
         my $out = $ele[0].".z.pz";
         system("strum2polzero $ele[1] $ele[7] $ele[8] $ele[9] $ele[10] $ele[11] > $dire/$out");
       }
       if($ele[4] == 1) {
         my $out = $ele[0].".n.pz";
         system("strum2polzero $ele[1] $ele[13] $ele[14] $ele[15] $ele[16] $ele[17] > $dire/$out");
       }
       if($ele[5] == 1) {
         my $out = $ele[0].".e.pz";
#        print "strum2polzero $ele[1] $ele[19] $ele[20] $ele[21] $ele[22] $ele[23]\n"; exit;
         system("strum2polzero $ele[1] $ele[19] $ele[20] $ele[21] $ele[22] $ele[23] > $dire/$out");
       }
     }
     # copy files with digital response if spin==2
     if("$ele[1]" eq "D") {
        my $inn = $ele[0].".".$ele[2].".Z.pz";
        my $out = $ele[0].".".$ele[2].".z.pz";
        system("/bin/cp $libre/$inn $dire/$out");
        my $inn = $ele[0].".".$ele[2].".N.pz";
        my $out = $ele[0].".".$ele[2].".n.pz";
        system("/bin/cp $libre/$inn $dire/$out");
        my $inn = $ele[0].".".$ele[2].".E.pz";
        my $out = $ele[0].".".$ele[2].".e.pz";
        system("/bin/cp $libre/$inn $dire/$out");
     }

     return ($ele[3],$ele[4],$ele[5]);;

 }


 sub SelectStaz {

     my $spi  = shift(@_);
     my $stas = shift(@_);
     my @sing = @_;
     my @ou   = ();
     my @staz = split('\_',$stas); 
     if($spi==0) {
       for(my $i=0; $i<=$#staz; $i++) {
          for(my $j=0; $j<=$#sing; $j++) {
             push(@ou,$staz[$i]) if("$staz[$i]" eq "$sing[$j]"); 
          }
       }
     }
     if($spi==1) {
       for(my $i=0; $i<=$#staz; $i++) {
          for(my $j=0; $j<=$#sing; $j++) {
             my @pppp = split(/([\_\/])/,$sing[$j]); 
             push(@ou,$sing[$j]) if("$pppp[$#pppp-2]" eq "$staz[$i]"); 
          }
       }
     }

     return @ou;

 } 

 sub ListStazWithRespo {

     my $sp = shift(@_);
     my @in = @_;
     my @ou = ();
     # first line == header
     if($sp == 0) {
        for(my $i=1; $i<=$#in; $i++) {
           chomp($in[$i]);
           my @f = split(' ',$in[$i]);
           push(@ou,$in[$i]) if("$f[1]" ne "0");
        }
     }
     if($sp == 1) {
        for(my $i=1; $i<=$#in; $i++) {
           chomp($in[$i]);
           my @f = split(' ',$in[$i]);
           push(@ou,$f[0]) if("$f[1]" ne "0");
        }
     }
     if($sp == 2) {
        for(my $i=1; $i<=$#in; $i++) {
           chomp($in[$i]);
           my @f = split(' ',$in[$i]);
           push(@ou,$f[0]) if("$f[1]" ne "0");
        }
     }
 
     return @ou;

 }

 sub MakeNewNameFiles {

     my @in = @_;
     for(my $i=0; $i<=$#in; $i++) {
        $in[$i] .= ".co";
     }
     return @in;

 }

 sub ReadFileList {

     my $pa = shift(@_);
     my $st = shift(@_);
     my @ou = ();
     opendir(DIR,"$pa"); my @dir=readdir(DIR), close(DIR);
     for(my $i=0; $i<=$#dir; $i++) {
        chomp($dir[$i]);
        my @f = split(/\./,$dir[$i]);
        next if("$dir[$i]" eq ".");
        next if("$dir[$i]" eq "..");
        # conditions
        next if("$f[$#f-1]" ne "$z");
        next if("$f[$#f-0]" eq "r");
        next if("$f[$#f-0]" eq "t");
        next if("$f[0]" ne "$st");
        push(@ou,$dir[$i]);
     }

     return @ou;
 }

sub MakenewDirs {

     my @in = @_;
     for(my $i=0; $i<=$#in; $i++) {
        $in[$i] .= "_co";
     }
     return @in;

 }


 sub GetAllStaz {

     my @dir = @_;
     my $z   = shift(@_);
     my @all = ();

     foreach $_ (@dir) {
        chomp($_);
        if("$_" eq "$z") {
#       my @tmp = split('',$_);
#       if("$tmp[0]" eq "0" && "$tmp[1]" eq "0" && $tmp[2] == $dept) {
#       print "$_\n";
        opendir(SBD,"$wrkp/$_"); my @sbd=readdir(SBD); close(SBD);
        for(my $j=0; $j<=$#sbd; $j++) {
           next if("$sbd[$j]" eq ".");
           next if("$sbd[$j]" eq "..");
           next if("$sbd[$j]" eq ".DS_Store");
           my @ttt = split(/\_/,$sbd[$j]);
           next if("$ttt[$#ttt]" ne "$z");
           push(@all,"$ttt[0]");   
       }
    }
  }

  return (@all);
 }

sub GetAllDirs {

     my $dic = shift(@_);
     my $z   = shift(@_);
     my @dir = @_;
     my @all = ();

#    my @Z = split(' '
     foreach $_ (@dir) {
        chomp($_);
        if("$_" eq "$z") {
#       my @tmp = split('',$_);
#       print "$_ $#tmp $dic ----- @tmp\n";
#       next if($#tmp!=$dic);
#       if("$tmp[0]" eq "0" && "$tmp[1]" eq "0" && $tmp[2] == $dept) {
#       print "$_\n";
        opendir(SBD,"$wrkp/$_"); my @sbd=readdir(SBD); close(SBD);
        for(my $j=0; $j<=$#sbd; $j++) {
           next if("$sbd[$j]" eq ".");
           next if("$sbd[$j]" eq "..");
           next if("$sbd[$j]" eq ".DS_Store");
           #remove sp and co directories
           my @f = split(/\_/,$sbd[$j]);
           next if("$f[$#f]" eq "sp");
           next if("$f[$#f]" eq "co");
           push(@all,"$wrkp/$_/$sbd[$j]");   
       }
    }
  }

  return (@all);

 } 

