#!/usr/bin/perl
 
 ##############################################################################
 #                                                                            #
 # Compute variance between pspectra of synthetics and observed               #
 #                                                                            #
 ##############################################################################

 use Env;
 use strict;
 use Math::FFT;
 use Math::VecStat qw(max min maxabs minabs sum average);
 use Math::Complex;
 use File::Path;
 use Array::Unique;
 use Statistics::Basic qw(:all);
 
 ##############################################################################
 # Arguments
 (my $yyyy, my $oblib, my $dept) = @ARGV;
 if($#ARGV != 2) {
   print "Usage: $0 year observed_file.inn depth\n";
   print "       $0 1917 1917_0426A.inn 5\n";
   exit;
 }
 my $z = sprintf("%03s",$dept);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200
 my $Tm; my $TM;
 my @variances;   # here al variances for single station and component
                  # structure: Sta_comp_NrMT[0-5000]
 my @DD;
 my @RR;

 ##############################################################################
 # Paths
 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $mtp   = "/Users/fabrizio/MTs/lib";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";
 my $obsf  = "/Users/fabrizio/MTs/work/$yyyy/observed";
 my $wrkd  = "/Users/fabrizio/MTs/work/$yyyy/$z";

 ##############################################################################
 # constants for fft
 my $NmaxPoints = 1024*2;
 my $tpi = 2.*acos(-1.0);
 my $tpi = 1;
 my $pi = 4 * atan2(1, 1);
 my $nor = 1;
 my $om;
 my @lin;
 my $T;
 my $NrMTRandom=5000;

 ##################################################################
 # Open and Read staz lib, and MT-work, and observed file, observed path
#open(OBS,"<$stp/$oblib"); my @obs=<OBS>; close(OBS);
 open(OBS,"<$oblib"); my @obs=<OBS>; close(OBS);
 opendir(DIR,"$wrkp"); my @dir=readdir(DIR); close(DIR);
 opendir(ODI,"$obsf"); my @odi=readdir(ODI); close(ODI);

 ##################################################################
 # Makename out file
 my @l = split(/\./,$oblib);
 my $outvar = $l[0].".var";
 open(OUT,">$outvar");
#open(OUT,">$wrkd/$outvar");

 print "$outvar\n";

 ##################################################################
 # Select Staz and paths with spectra: output of do_04
 my @alldirs = GetAllDirs($dic,$z,@dir);

 #################################################################
 my $date_event = $obs[0];
 my $surs_event = $obs[1];

 #################################################################
 # Now make loop over stations and components of observed file
 # In obsfiles I have all files observed to compute fft


 my @obsfiles=();
 my @periods=();
 for(my $i=2; $i<=$#obs; $i++) {
  
    chomp($obs[$i]);
    my @lin = split(' ',$obs[$i]);
              # lin[0]     = staz
              # lin[1]     = chan
              # lin[2]     = spin staz
              # lin[3]     = spin z
              # lin[4]     = spin n
              # lin[5]     = spin e
              # lin[6]     = diff phase z unused 
              # lin[7]     = diff phase n unused 
              # lin[8]     = diff phase e unused
              # lin[9-10]  = Min_Max T z
              # lin[11-12] = Min_Max T n
              # lin[13-14] = Min_Max T e
              # lin[15]    = spin polarity
              # lin[16-18] = polarity z n e
   
    next if($lin[2]==0);  #next if station is off for spectra analysis

    for(my $k=0; $k<=2; $k++) {  #loop over components

      next if($lin[3+$k]==0); #next if component is of
      my $co;
      $co = "z" if(3+$k==3);
      $co = "n" if(3+$k==4);
      $co = "e" if(3+$k==5);

      # Read Name files
      my $oo = MakeNameObs($lin[0],$lin[1],$co);
      push(@obsfiles,$oo);

      # Read Period ranges
      my $tz;
      my $Tz;
      my $TTz;
      chomp($obs[0]);
      my @ll = split(' ',$obs[0]);
      if($ll[6]==1) {  # One period range for all traces
         $tz=$ll[7]; 
         $Tz=$ll[8];
         $TTz = $tz."_".$Tz;
         push(@periods,$TTz);
      }
      else {
        # Read Periods for each trace
        $tz = $lin[10+$k*2];
        $Tz = $lin[11+$k*2];
        $TTz = $tz."_".$Tz;
        push(@periods,$TTz);
      }

    }


 }

 #################################################################
 # Now loop over @obsfiles
 # 1. make fft and spectra
 # 2. locate first MT synt of that station and comp
 # 3. read pws
 # 4. Make single variance
 # 5. make single variance in @Variances[0-5000] with n_observed values for each i
 # 6. compute Vari_mean in #Vari_mean[0-5000], single value for i 
 my @VARI=();
#my @DD=();
#my @RR=();
 for(my $k=0; $k<=$#obsfiles; $k++) { #loop over MT

       # take periods
       my @Ts = split('_',$periods[$k]);
       $Ts[0]=sprintf("%.1f",$Ts[0]);
       $Ts[1]=sprintf("%.1f",$Ts[1]);

       (my $dtd,my @syn) = ReadSac($obsf,$obsfiles[$k]);
#      print "$obsf/$obsfiles[$k]  $syn[200] $syn[270] $syn[300] $syn[320] $syn[330] $syn[350]\n";

       # array to vector
       my $syn = \@syn;

       # make fft
       my $syn_fft   = new Math::FFT($syn);
       my $syn_spctr = $syn_fft->spctrm;

       # Set frequencies and periods
       my $dom   = 1/($NmaxPoints*$dtd);

       # Normalize and write in array 
       my $max = max($syn_spctr);
       @lin = ();
       for(my $i = 1; $i<$#syn/2; $i++) {
          $syn_spctr->[$i] = $syn_spctr->[$i]*$nor/$max;
          $om = $dom * $i * $tpi;
          $T  = 1/$om;

     
          if($T >= $Ts[0] && $T <= $Ts[1]) {
            my $a = sprintf("%e",$syn_spctr->[$i]);
#           my $a = sprintf("%5d  %e  %e  %e",$i,$om,$T,$syn_spctr->[$i]);
#           print "$obsfiles[$k] *** $a *** $Ts[0] $T $Ts[1]\n";
            push(@lin,$a);
          }
       }


       # Now find sp_dir (froim alldirs) and open sp files, and compute variance
#      print "$obsfiles[$k]\n";
       my @lll = split(/\./,$obsfiles[$k]);
       my @pa=grep {/$lll[0]/} @alldirs;
       my $path_sp = $pa[0];   #here where to find file 
       my $compone = $lll[3];
 
       # Here files to read and make variance
       my @files_synt = ListSyntFiles($path_sp,$compone);
       $NrMTRandom=$#files_synt;
       print ">>>> $NrMTRandom $path_sp $compone \n";

       # loop over files
       my $DD; my $RR;
       for(my $j=0; $j<=$#files_synt; $j++) {
          # here data syntetics os pwsp
          my @data = ReadDataSynt($path_sp,$files_synt[$j],$Ts[0],$Ts[1]);
#         print "$path_sp $files_synt[$j] \n";
   
          
          #make variance 
          my $dd;
          my $rr;
          my $Dd;
          my $Rr;
          for(my $n=0; $n<=$#lin; $n++) {
             $Dd += ($lin[$n])**2;
             $Rr += ($lin[$n]-$data[$n])**2;
             $dd += ($lin[$n])**2;
             $rr += ($lin[$n]-$data[$n])**2;
          }
          my $vari=sprintf("%.6f",$rr/$dd);
          push(@variances,$vari);
          push(@DD,$Dd);
          push(@RR,$Rr);
#         print ">> $obsfiles[$k] $files_synt[$j] $vari\n";
       }

 } #End loop over @observed files 


 ##################################################################
 # Now for each Mt solution [0-5000] compute mean variance
 # Put in vector Mean_viriances with order of obsfiles
 # remindres: my @variances;   # here al variances for single station and component
                  # structure: Sta_comp_NrMT[0-5000]
                  
 my $meanVari;
 my @Var;
 my @out_variances_spectra = ();

 print ">>>> $NrMTRandom\n";
 for(my $j=0; $j<=$NrMTRandom; $j++) {
    @Var=();
    my $D;
    my $R;
    for(my $i=0; $i<=$#obsfiles;$i++) {
       push(@Var,$variances[$i*($NrMTRandom+$j+1)]);
       $D += $DD[$i*($NrMTRandom+$j+1)];
       $R += $RR[$i*($NrMTRandom+$j+1)];
    }
    my $VARI = sprintf("%.6f",$R/$D);
    my $mean_var=sprintf("%10.5f",mean(@Var));
    my $stda_var=sprintf("%10.5f",stddev(@Var));
    my $Nr = sprintf("%7d",$j);
    my $ou = $mean_var."_".$stda_var;
#   print ">>> $j $VARI $stda_var\n"; 
    print OUT "$j $mean_var $stda_var $VARI\n";
#   push(@out_variances_spectra,$ou);
 }

 close(OUT);


 ##################################################################
 ##################################################################

 sub ReadDataSynt {

     my $pa = shift(@_);
     my $fi = shift(@_);
     my $Tm = shift(@_);
     my $TM = shift(@_);
     my @ou;

     open(FI,"<$pa/$fi"); my @fi=<FI>; close(FI);
     shift(@fi);
     foreach $_ (@fi) {
       chomp($_);
       my @l = split(' ',$_);
       push(@ou,$l[3]) if($l[2] >= $Tm && $l[2] <= $TM);
       last if($l[2]<$Tm);
     }
     return @ou;

 }

 sub ListSyntFiles {

     my $pa = shift(@_);
     my $co = shift(@_);
     my @out;
     opendir(DIR,"$pa"); my @dir=readdir(DIR); close(DIR);
     foreach $_ (@dir) {
       chomp($_);
       my @l = split(/\./,$_);
       push(@out,$_) if("$l[4]" eq "$co");
     } 
     return @out;

 }


 sub MakeNameObs {

     my $sta = shift(@_);
     my $cha = shift(@_);
     my $con = shift(@_);
     my @f = split(/\//,$oblib);
#    my $data = substr($oblib,0,9);
     my $data = substr($f[$#f],0,9);

     my $out;

     $out = $sta.".".$cha.".".$data.".".$con.".sac";


     return $out;

 }

 ##################################################################

 sub GetAllDirs {

     my $dic = shift(@_);
     my $z   = shift(@_);
     my @dir = @_;
     my @all = ();

     foreach $_ (@dir) {
        chomp($_);
        if("$_" eq "$z") {
#       my @tmp = split('',$_);
#       next if($#tmp!=$dic);
#       if("$tmp[0]" eq "0" && "$tmp[1]" eq "0" && $tmp[2] == $dept) {
        opendir(SBD,"$wrkp/$_"); my @sbd=readdir(SBD); close(SBD);
        for(my $j=0; $j<=$#sbd; $j++) {
           next if("$sbd[$j]" eq ".");
           next if("$sbd[$j]" eq "..");
           next if("$sbd[$j]" eq ".DS_Store");
           my @f = split(/\_/,$sbd[$j]);
           next if("$f[$#f]" ne "sp");
           push(@all,"$wrkp/$_/$sbd[$j]");
       }
    }
  }

  return (@all);

 }

 sub ReadSac {

     my $pa = shift(@_);
     my $st = shift(@_);
     my @ou = ();

     open(FH,"<$pa/$st") or print "no $pa/$st found\n";; binmode(FH);
     my $npts  = ReadValue(79,4,'i');
     my $delta = ReadValue(0,4,'f');
     if($npts<$NmaxPoints) {
        for(my $i=158; $i<=158+$npts; $i++) {
           my $a=ReadValue($i,4,'f');
           push(@ou,$a);
        }
        for(my $i=0; $i<=$NmaxPoints-$npts-2; $i++) {
           push(@ou,0);
        }
     }
     if($npts>=$NmaxPoints) {
        for(my $i=158; $i<=158+$NmaxPoints-1; $i++) {
           my $a=ReadValue($i,4,'f');
           push(@ou,$a);
        }

     }
     return ($delta,@ou);

 }

 sub ReadValue {

   seek(FH, @_[0]*@_[1],0);
   read(FH, my $tmp, @_[1]);
   $tmp=unpack(@_[2], $tmp);
   return $tmp;

 }

