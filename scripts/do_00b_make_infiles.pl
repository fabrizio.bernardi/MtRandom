#!/usr/bin/perl

  ##############################################################
  #                                                            #
  # Make inputfiles_for MtrandoV40                             #
  #                                                            #
  ##############################################################

  use Env;
  use strict;

  if($#ARGV != 7) {
     print "Usage: $0 YYYY_station_file.inn YYYY_MMDDA.inn MTi\n";
     print "       $0 2003_0914_station_file.inn 2003_0914A.inn Mtt ...\n";
     exit;
  }
  my $inf1 = shift(@ARGV);
  my $inf2 = shift(@ARGV);
  my @mti  = @ARGV; 

  ##############################################################
  # Define Earth model: 1=iasp91, 2=ak135
  my $model = 1;
  my $gpol = "/Users/fabrizio/MTs/bin/mtrandomV4.0/get_polarity.pl";
  my $pp   = "/Users/fabrizio/lib/staz_mt_random";

  ##############################################################
  # make yyyy_station_file.inn
  # First add dlaz output and take_off angle
  #
  # --- Open and read original yyyy_station_file.inn
  # --- oputput on the same file
  print "<$pp/$inf1\n";
  open(INF1,"<$pp/$inf1") or die $!; my @in1=<INF1>; close(INF1);
# open(INF2,">$inf2") or die $!; my @in2=<INF2>; close(INF2);
  
  for(my $i=1; $i<=$#in1; $i++) {
      chomp($in1[$i]);
      my @l = split(' ',$in1[$i]);
      my $sta = $l[0];
      my $azi = $l[5];
      my $aoi = $l[9];
#    print "$gpol @mti $aoi $azi\n";
      open(P,"$gpol @mti $aoi $azi |"); my @p=<P>; close(P);
      chomp($p[0]);
      my $s  = sprintf("%-5s",$sta);
         $s .= "B  0  1  1  1  1  0  0  0 005 035 005 035 005 035  1 $p[0]\n"; 
      print "$s";

  }

