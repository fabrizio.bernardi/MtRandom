#!/usr/bin/perl

 ##################################################################
 #                                                                #
 # This read the Sinthetics directories, and read Synt and derive #
 # polarities                                                     #
 #                                                                #
 ##################################################################

 use strict;
 use Env;
 use Statistics::Basic qw(:all);

#my $stlib = "1917_station_file.inn";
 (my $yyyy, my $stlib, my $oblib, my $rotf, my $dept) = @ARGV;
 if($#ARGV != 4) {
   print "Usage: $0 year station_file.inn observed_file.inn rotation.inn depth\n";
   print "       $0 1917 1917_station_file.inn 1917_0426A.inn 1917_rotatio_file.inn 5\n";
   exit;
 }

 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $mtp   = "/Users/fabrizio/MTs/lib";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";
 my $meas  = "run_check_polarity_of_syntetics.pl";
 my $mtlib = "mtlib_3.1_05000.lib";

 my $z = sprintf("%03s",$dept);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200


 
 ##################################################################
 # Open and Read staz lib, and MT-work
 open(OBS,"<$oblib"); my @obs=<OBS>; close(OBS);
 open(ROT,"<$rotf"); my @rot=<ROT>; close(ROT);
 open(STA,"<$stlib"); my @sta=<STA>; close(STA);
#open(OBS,"<$stp/$oblib"); my @obs=<OBS>; close(OBS);
#open(ROT,"<$stp/$rotf"); my @rot=<ROT>; close(ROT);
#open(STA,"<$stp/$stlib"); my @sta=<STA>; close(STA);
 shift(@sta);
 opendir(DIR,"$wrkp"); my @dir=readdir(DIR); close(DIR);

 ##################################################################
 # read from obs_file station to be rotated, spin[9] = 1
 my @rotation_stations=getRotationStaz(@obs);


 ##################################################################
 # associate the rotation to each station
 for(my $i=0; $i<=$#rotation_stations; $i++) {
     $rotation_stations[$i]=GetRotationAngle($rotation_stations[$i],@rot);
 }

 ##################################################################
 #Select Staz and paths
 my @alldirs = GetAllDirs($dic,$z,@dir);
 my @allstaz = @sta;

 
 ##################################################################
 # Make string to call for each station
 # questo ok, ma cambiare e diamo come input azimuth e aoi
 my @strings=();
 for(my $j=0; $j<=$#allstaz; $j++) {
    chomp($allstaz[$j]);
    my @l = split(' ',$allstaz[$j]);
    my $azi = $l[6];
    my $Paoi= $l[10];
    my $dire  = Makedir($l[0],@alldirs); 
#   my $rotation = CheckIfRoatation($allstaz[$j],@allstaz);
    my $rotation = CheckIfRoatation($l[0],@rotation_stations);
    push(@strings,"$dire $l[0] $azi $Paoi $rotation");
 }



 ##################################################################
 # Call amplitude measurements
 foreach $_ (@strings) {

   print "$meas $_\n";
   system("$meas $_");


 }

 
 ##################################################################
 ##################################################################


 sub CheckIfRoatation {

     my $sta = shift(@_);
     my @rot = @_;
     my $out = 0;
     foreach $_ (@rot) {
        my @f = split(/\_/,$_);
        if("$sta" eq "$f[0]") {
          $out = $f[1];
        }
     }
     return $out;

 }

 sub GetRotationAngle {

     my $sta = shift(@_);
     my @rot = @_;

     for(my $i=0; $i<=$#rot; $i++) {
         chomp($rot[$i]);
         my @f = split(' ',$rot[$i]);
         if("$sta" eq "$f[0]") {
            $sta = $sta."_".$f[1];
            last;
         }
     }
     return $sta;


 }

 sub getRotationStaz {

     my @in = @_;
     my @ou;
     for(my $i=2; $i<=$#in; $i++) {
         chomp($in[$i]);
         my @f=split(' ',$in[$i]);
         push(@ou,$f[0]) if($f[9]==1);
     }
     return @ou;

 }


 sub Makedir {

     my $st = shift(@_);
     my @di = @_;
     my @ou = grep {/$st/} @di;
     chomp($ou[0]);
     return $ou[0];

 }

 sub GetAllDirs {

     my $dic = shift(@_);
     my $z   = shift(@_);
     my @dir = @_;
     my @all = ();

     foreach $_ (@dir) {
        chomp($_);
        if("$_" eq "$z") {
#       my @tmp = split('',$_);
#       next if($#tmp!=$dic);
#       if("$tmp[0]" eq "0" && "$tmp[1]" eq "0" && $tmp[2] == $dept) {
        opendir(SBD,"$wrkp/$_"); my @sbd=readdir(SBD); close(SBD);
        for(my $j=0; $j<=$#sbd; $j++) {
           next if("$sbd[$j]" eq ".");
           next if("$sbd[$j]" eq "..");
           next if("$sbd[$j]" eq ".DS_Store");
           my @f = split(/\_/,$sbd[$j]);
           next if("$f[$#f]" eq "co");
           next if("$f[$#f]" eq "sp");

           push(@all,"$wrkp/$_/$sbd[$j]");
       }
    }
  }

  return (@all);

 }

