#!/usr/bin/perl

  ##############################################################
  #                                                            #
  # Make inputfiles_for MtrandoV40                             #
  #                                                            #
  ##############################################################

  use Env;
  use strict;

  my $inf1 = $ARGV[0];
  if($#ARGV != 0) {
     print "Usage: $0 YYYY_station_file.inn\n";
     print "       $0 1917_station_file.inn\n";
     exit;
  }

  ##############################################################
  # Define Earth model: 1=iasp91, 2=ak135
  my $model = 1;

  ##############################################################
  # make yyyy_station_file.inn
  # First add dlaz output and take_off angle
  #
  # --- Open and read original yyyy_station_file.inn
  # --- oputput on the same file
  open(INF1,"<$inf1") or die $!; my @in=<INF1>; close(INF1);
  open(OUF1,">$inf1");
  
  # --- read hypocenter coordinates
  # --- and revrite first hypocenter line on output file
  chomp($in[0]);
  print OUF1 "$in[0]\n";
  my @l=split(' ',$in[0]);
  my $lat_e = $l[1];
  my $lon_e = $l[2];
  my $dep_e = $l[3];
  # 
  # --- begin loop over stations
  for(my $i=1; $i<=$#in; $i++) {
     chomp($in[$i]);
     @l=split(' ',$in[$i]);
     my $lat_s = $l[2];
     my $lon_s = $l[3];

     # distances and azimuth
     open(DL,"dlaz $lat_e $lon_e $lat_s $lon_s |"); my @dl=<DL>; close(DL);
     chomp($dl[0]);
     my @t=split(' ',$dl[0]);

     # take_off angle
     open(DL,"get_aoi << eof
              $lat_e $lon_e $dep_e $lat_s $lon_s 
              $model
              eof |"); my @dl=<DL>; close(DL);
     chomp($dl[3]);
     #aoi_P = $a[2]
     #aoi_S = $a[3]
     my @a=split(' ',$dl[3]);

     # rewrite output
     my $s = sprintf("%-5s %1d %7.3f %7.3f %9.4f %8.4f%9.4f%8.2f%8.2f 0 %6.3f %6.3f",
                     $l[0],$l[1],$l[2],$l[3],$l[4],$t[0],$t[1],$t[2],$t[3],$a[2],$a[3]);
     print OUF1 "$s\n";
     
  }
  
  
