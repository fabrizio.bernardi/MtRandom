#!/usr/bin/perl

 #######################################################################
 #                                                                     #
 # Give numer of MT in mtrandom_process and return MT values from lib  #
 #                                                                     #
 #######################################################################


 use strict;
 use Env;

 exit if($#ARGV != 0);
 my $id = $ARGV[0];

 my $mtpa = "/Users/fabrizio/MTs/lib";
 my $mtlb = "mtlib_3.1_05000.lib"; 
#my $mtlb = "mt_lib_n10000_ID0000.lib"; 

 open(MT,"<$mtpa/$mtlb"); my @mt=<MT>; close(MT);

 for(my $i=0; $i<=9;$i++) {
    chomp($mt[$id*10+$i]);
    print "$mt[$id*10+$i]\n";
 } 
