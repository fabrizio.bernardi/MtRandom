#!/usr/bin/perl

 ##############################################################################
 #                                                                            #
 # Make conbvolution between stations and MT and Green for big_libs           #
 # Updates                                                                    #
 # - Put comvoluted seismograms into: /Users/fabrizio/MTs/work/YYYY and not in#
 #                                    /Users/fabrizio/MTs/work                #
 #   This allow the run of multiple events                                    # 
 # 									      #
 ##############################################################################

 use strict;
 use Env;
 use File::chdir;
 use File::Path;

 ##############################################################################
 # SEt pth and variables
 if($#ARGV != 2) {
    print "Usage: $0 depth station_file_inn year\n";
    print "       $0 5 1917_station_file.inn ";
    print           "[/Users/fabrizio/lib/staz_mt_random] 1917\n";
    exit; 
 }
 my $depth = $ARGV[0];
 my $satzF = $ARGV[1];
 my $subPY = $ARGV[2];
 my $workp = "/Users/fabrizio/MTs/work";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $stazp = "/Users/fabrizio/lib/staz_mt_random";
 my $mtpat = "/Users/fabrizio/MTs/lib";
 my $mtlib = "mtlib_3.1_05000.lib";
 my $Run_Synt = "run_Synt_forMT.pl";
 my @dwidt = (200,2600);
 my $z     = sprintf("%03d",$depth);
 # Elements of dfile (input file for surf96)
 # see manual for details
 my $ddis;
 my $dt    = 0.5;
 my $npts  = 2048;
 my $df0   = 0;
 my $red   = 10;
 # end elements of dfile
 # Elements for Earth model
 my $EarthM = "/Users/fabrizio/MODELS/general/SURF96/ak135_sphe_34lyers.mod";
 my $NrModes= 2000;
 my $Mo     = 1e25;
 
 ####################################################################################
 # Station file and MT libs
#open(STA,"<$stazp/$satzF"); my @sta=<STA>; close(STA);
 # now give path
 open(STA,"<$satzF"); my @sta=<STA>; close(STA);
 my @coo_e = GetCoordinatesEvent($sta[0],$z);
 shift(@sta);
 open(LIB,"<$mtpat/$mtlib"); my @lib=<LIB>; close(LIB);
 @lib=SelectMTs(@lib);
  
 ####################################################################################
 # Begin loop
 # 1: Read station distance, azimuth (and station coordinates)
 for(my $k=0; $k<=$#sta; $k++) {

    # Station info
    chomp($sta[$k]);
    my @station_elements = split(' ',$sta [$k]);
    
    my $POL = $station_elements[1];
    next if($POL==0); 

    my $ST = $station_elements[0];
    my $la = $station_elements[2];
    my $lo = $station_elements[3];
    my $az = $station_elements[5];
    my $km = $station_elements[8];

    # Do not proceed if station is too close or to faraway
    if($km < $dwidt[0] || $km > $dwidt[1]) {
      print "Station $ST has distance $km, exit!!\n";
      next;
    }

    # Make path for computation and storage of synthetics 
    my $st_d = $ST."_".$z;
    system("mkdir -p $workp/$subPY/$z/$st_d"); 
    print "$workp/$subPY/$z/$st_d\n";

    #Change dir
    chdir("$workp/$subPY/$z/$st_d");

    #Make station files
    my $dfile = "dfile.$ST";
    open(DF,">$workp/$subPY/$z/$st_d/$dfile");
      print DF "$km $dt $npts $df0 $red\n";
    close(DF);

    # 1. produce green functions
    my $str_sprep96  = "-M $EarthM -d $dfile -L -R -NMOD $NrModes ";
       $str_sprep96 .= "-HS $depth -HR 0\n"; 
    open(STDOUT,">$workp/$subPY/$z/$st_d/green.log");
    system("sprep96 $str_sprep96 >&STDOUT");
    system("sdisp96 >&STDOUT ");
    system("sregn96 >&STDOUT ");
    system("slegn96 >&STDOUT ");
 
    # 2. loop over mt elements to produce syntetics
    for(my $i=0; $i<=$#lib; $i++) {

       chomp($lib[$i]);
       my @mt=split(' ',$lib[$i]);
       shift(@mt);
       # Mo*mti
       for(my $j=0; $j<=$#mt; $j++) {$mt[$j] *= $Mo}

       # make name synthetic files
       my @names=FormatFileName($mtlib,$i,$ST,$depth);

       # prepare convolution
       my $mtconvolution  = "spulse96 -p -V -l 4 -d $dfile | ";
          $mtconvolution .= "fmech96 -XX $mt[1] -YY $mt[2] -ZZ $mt[0] ";
          $mtconvolution .= "-XY $mt[5] -XZ $mt[3] -YZ $mt[4] -A $az | ";
          $mtconvolution .= "f96tosac -B \n";
      
       # apply convolution
       system("$mtconvolution >&STDOUT");

       #rename name files
       system("mv $workp/$subPY/$z/$st_d/B00101Z00.sac $workp/$subPY/$z/$st_d/$names[0]");
       system("mv $workp/$subPY/$z/$st_d/B00102N00.sac $workp/$subPY/$z/$st_d/$names[1]");
       system("mv $workp/$subPY/$z/$st_d/B00103E00.sac $workp/$subPY/$z/$st_d/$names[2]");

    }
    
 }





 ###################################################################################
 ###################################################################################

 sub FormatFileName {

     my $nlib = shift(@_);
     my $Id   = sprintf("%05d",shift(@_));
     my $sta  = shift(@_);
     my $dept = sprintf("%03d",shift(@_));

     my @tmp = split('\.',$nlib);
     my @ou  = ();
     $ou[0]= $sta.".".$tmp[0].".".$Id.".".$dept.".z";
     $ou[1]= $sta.".".$tmp[0].".".$Id.".".$dept.".n";
     $ou[2]= $sta.".".$tmp[0].".".$Id.".".$dept.".e";

     return @ou ;

 }


 sub SelectMTs {

     my @in = @_;
     my @ou = ();
     foreach $_ (@lib) {
       my @tmp = split(' ',$_);
       chomp($_);
       push(@ou,$_) if("$tmp[0]" eq "M3");
     }
     return @ou;

 }

 sub GetCoordinatesEvent {

    my $e = shift(@_);
    chomp($e);
    my $z = shift(@_);
    my @l = split(' ',$e);
    return ($l[1],$l[2],$z);

 }

 ##########################################################################################

 sub GETNRLIB {

    my @foe = split(/([._D])/,$_[0]);
    my $nr = sprintf("%d",$foe[$#foe]);
    return $nr;

 }


###########################################################################################

#sub selectMTlibs {

#   my @out = ();
#   my $m = shift(@_);
#   my $M = shift(@_);
#   my @in = @_; 
#   

#   for(my $j=0; $j<=$#in; $j++) {

#      chomp($in[$j]);
#      next if("$in[$j]" eq ".");
#      next if("$in[$j]" eq "..");

#      my @tt = split(/\./,$in[$j]);
#      next if("$tt[$#tt]" ne "lib");
#      my $nr = GETNRLIB($tt[$#tt-1]);
#      push(@out,$in[$j]) if($nr >= $m && $nr <= $M);

#   }
#   return @out;
#
#}



#cd /Users/fabrizio/MTs/work/GTT_22
#cp /Users/fabrizio/MTs/work/GTT_04/GTT_StazFile .
#../../bin/Run_Synt.pl GTT  893 350.6 22 mt_lib_n10000_ID0001.lib . . v GTT_StazFile
#exit
