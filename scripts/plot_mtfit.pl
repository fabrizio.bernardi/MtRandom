#!/usr/bin/perl

 ##################################################################################
 #                                                                                #
 # Plot mt-random solution and power spectra fit                                  #
 #                                                                                #
 ##################################################################################

 use Env;
 use strict;
 use Math::FFT;
 use Math::VecStat qw(max min maxabs minabs sum average);
 use Math::Complex;
 use File::Path;
 use Array::Unique;
 use Statistics::Basic qw(:all);


 (my $yyyy, 
  my $dept, 
  my $stazfil, 
  my $oblib, 
  my $outfile, 
  my $id,
  my $frm,
  my $frM,
  my $Pwm,
  my $PwM,
  my $NrMt) = @ARGV;
 if($#ARGV != 10) {
   print "Usage: $0 year observed_file.inn depth outfile IDnumber Plot_ranges [Fr Pw] NrMtRandom\n";
   print "       $0 1917 5 1917_station_file.inn 1917_0426A.inn ";
   print " 1917_0426AV.out test001 0.02 0.1 1e-7 1 234\n";
   exit;
 }
 my $z   = sprintf("%03s",$dept);
 my $mtnr= sprintf("%05s",$NrMt);
 my $dic = 2;    #number of digit of the Z dir: 005 010 015 .. 200

 ##############################################################################
 # Paths
 my $wrkp  = "/Users/fabrizio/MTs/work/$yyyy";
 my $binp  = "/Users/fabrizio/MTs/bin/mtrandomV4.0";
 my $mtp   = "/Users/fabrizio/MTs/lib/mtlib_3.1_05000.lib";
 my $libre = "mtlib_3";
 my $stp   = "/Users/fabrizio/lib/staz_mt_random";
 my $obsf  = "/Users/fabrizio/MTs/work/$yyyy/observed";
 my $wrkd  = "/Users/fabrizio/MTs/work/$yyyy/$z";

 ##############################################################################
 # constants for fft
 my $NmaxPoints = 1024*2;
 my $tpi = 2.*acos(-1.0);
 my $tpi =1;
 my $pi = 4 * atan2(1, 1);
 my $nor = 1;
 my $om;
 my @lin;
 my $T;
 my $NrMTRandom=5000;

 ##################################################################
 #########  Begin  Preparation data_plot

 ##################################################################
 # Open and Read staz lib, and MT-work, and observed file, observed path
 open(OBS,"<$oblib"); my @obs=<OBS>; close(OBS);
 open(INN,"<$stazfil"); my @inn=<INN>; close(INN);
 open(RIT,"<$outfile"); my @rit=<RIT>; close(RIT);
#open(OBS,"<$stp/$oblib"); my @obs=<OBS>; close(OBS);
#open(INN,"<$stp/$stazfil"); my @inn=<INN>; close(INN);
#open(RIT,"<$wrkd/$outfile"); my @rit=<RIT>; close(RIT);
 opendir(DIR,"$wrkp"); my @dir=readdir(DIR); close(DIR);
 opendir(ODI,"$obsf"); my @odi=readdir(ODI); close(ODI);


 ##################################################################
 # Select Staz and paths with spectra: output of do_04
 my @alldirs = GetAllDirs($dic,$z,@dir);

 #################################################################
 my $date_event = $obs[0];
 my $surs_event = $obs[1];

 #################################################################
 # Now make loop over stations and components of observed file
 # In obsfiles I have all files observed to compute fft
 my @obsfiles=();
 my @periods=();
 for(my $i=2; $i<=$#obs; $i++) {

    chomp($obs[$i]);
    my @lin = split(' ',$obs[$i]);
              # lin[0]     = staz
              # lin[1]     = chan
              # lin[2]     = spin staz
              # lin[3]     = spin z
              # lin[4]     = spin n
              # lin[5]     = spin e
              # lin[6]     = diff phase z unused 
              # lin[7]     = diff phase n unused 
              # lin[8]     = diff phase e unused
              # lin[9-10]  = Min_Max T z
              # lin[11-12] = Min_Max T n
              # lin[13-14] = Min_Max T e
              # lin[15]    = spin polarity
              # lin[16-18] = polarity z n e

    next if($lin[2]==0);  #next if station is off for spectra analysis

    for(my $k=0; $k<=2; $k++) {  #loop over components

      next if($lin[3+$k]==0); #next if component is of
      my $co;
      $co = "z" if(3+$k==3);
      $co = "n" if(3+$k==4);
      $co = "e" if(3+$k==5);

      # Read Name files
      my $oo = MakeNameObs($lin[0],$lin[1],$co, $yyyy);
      push(@obsfiles,$oo);

      # Read Period ranges
      my $tz;
      my $Tz;
      my $TTz;
      chomp($obs[0]);
      my @ll = split(' ',$obs[0]);
      if($ll[6]==1) {  # One period range for all traces
         $tz=$ll[7];
         $Tz=$ll[8];
         $TTz = $tz."_".$Tz;
         push(@periods,$TTz);
      }
      else {
        # Read Periods for each trace
        $tz = $lin[10+$k*2];
        $Tz = $lin[11+$k*2];
        $TTz = $tz."_".$Tz;
        push(@periods,$TTz);
      }

    }

 }

 # select stations from obs files
 my @staz = SelectStations(@obsfiles);
 my @comp = SelectComponents(@obsfiles);
 my @spfi = makeNameSpfiles(@obsfiles);   #sp_name files for observed
 my @synp = SelectSynPath(@staz);         #path for synfiles
 my @synsp = FindSynSpFiles(@staz,@comp); # find synt_sp files

 # Find Stations info
 my @azi  = ReadStazPar(0,@staz);
 my @dis  = ReadStazPar(1,@staz);
 my @aoi  = ReadStazPar(2,@staz);

 # Find mt[i] elements  Planes mt[i]  Axes mt[i]   epsilon mt[i]
 my @mts = FindParameters($NrMt,2);
 my @pla = FindParameters($NrMt,5);
 my @axe = FindParameters($NrMt,7);
 (my $mo, my $eps) = FindParameters($NrMt,4);

 # find fit variances for mt[i]
 open(VAR,"<$wrkd/$outfile"); my @var=<VAR>; close(VAR);
 chomp($var[$NrMt]);
 my @fit=split(' ',$var[$NrMt]);
 my $fit_pol = $fit[1];
 my $fit_var = $fit[2]; 
 my $fit_std = $fit[3];
 my $fit_vec = $fit[5];
 my $fit_rel = $fit[6];

 # find earthquake info
 (my $lat, my $lon, my $hypo, my $location, my @date) = GetInfo($inn[0],$obs[0]);

 # Foreach obsfiles make fft , power spectra and write files
 for(my $k=0; $k<=$#obsfiles; $k++) {

      # read files
      (my $dtd,my @syn) = ReadSac($obsf,$obsfiles[$k]);
     
       # array to vector
       my $syn = \@syn;

       # make fft
       my $syn_fft   = new Math::FFT($syn);
       my $syn_spctr = $syn_fft->spctrm;

       # Set frequencies and periods
       my $dom   = 1/($NmaxPoints*$dtd);

       # Normalize and write in array 
       my $max = max($syn_spctr);
       @lin = ();
       for(my $i = 1; $i<$#syn/2; $i++) {
          $syn_spctr->[$i] = $syn_spctr->[$i]*$nor/$max;
          $om = $dom * ($i+0) * $tpi;
          $T  = 1/$om;
          my $a = sprintf("%5d  %e  %e  %e",$i,$om,$T,$syn_spctr->[$i]);
          push(@lin,$a);
       }

       # write sp files
       open(OU,">$obsf/$spfi[$k]");
          print OU "       Freq[Hz]      T[s]          PwSpr\n";
          foreach $_ (@lin) {
             print OU "$_\n";
          }
       close(OU);

  }
 
 ######### End Preparation data_plot
 ##################################################################


 ##################################################################
 ######### Begin generation of gmt script
 ##################################################################

  ### earthquake location
  ### earthquake parameters
  ### bbs 
  ### Staz, Dist, azi, aoi, Fit

  ### First name ps file
  (my $outps, my $outpdf) = MakeNameOutfilePs($oblib,$NrMt,$z,$id);
  my $gmtp   = "$wrkp/plots";
  my $gmtstr = "gmtset ANNOT_FONT_SIZE         = 6p \n";
     $gmtstr.= "gmtset LABEL_FONT_SIZE         = 10p\n";
     $gmtstr.= "gmtset LABEL_OFFSET            = 0.001i\n";
     $gmtstr.= "gmtset HEADER_FONT_SIZE        = 7p\n";
     $gmtstr.= "gmtset HEADER_OFFSET           = 0.081i\n";
     $gmtstr.= "gmtset CHAR_ENCODING           = ISOLatin1+\n";

  ### Write header, with earthquake info and main parameters solution
  $gmtstr .= "pstext << END -JX2.12/19.5 -R0/10/0/5 -P -N -K -X0.1 -Y26.5 > $gmtp/$outps\n";
  $gmtstr .= "2 0 12 0 5 5 $location\n";
  $gmtstr .= "2 -0.13 10 0 5 5 Date: $date[2].$date[1].$date[0] Time:$date[3]:$date[4]:$date[5] ";
  $gmtstr .=                 " Lat:$lat Long:$lon Depth:$dept\n";
  $gmtstr .= "2 -0.26 10 0 5 5 Plane (strike/dip/slip)  NP1: $pla[0]/$pla[1]/$pla[2] ";
  $gmtstr .=                                           "NP2: $pla[3]/$pla[4]/$pla[5]\n";
  $gmtstr .= "2 -0.39 10 0 5 5 Axes (Val/Plg/Azi) T: $axe[0]/$axe[1]/$axe[2] ";
  $gmtstr .=                                     "N: $axe[3]/$axe[4]/$axe[5] ";
  $gmtstr .=                                     "P: $axe[6]/$axe[7]/$axe[8]\n";
  $gmtstr .= "2 -0.52 10 0 5 5 Variance - Std PWSP:  $fit_var - $fit_std ";
  $gmtstr .=                  "Fit P-polarity : $fit_pol ";
  $gmtstr .=                  "Vector fit : $fit_vec\n";
  $gmtstr .= "2 -0.65 10 0 5 5 MT-random Nr: $NrMt\n"; 
  $gmtstr .= "END\n";

  ### Write bbs
  # -- write azi.tmp file
  my @azim = @azi;
  open(TMP,">$gmtp/azi.tmp");
  for(my $i=0; $i<=$#azim; $i++) {
     if ($azim[$i]<=90) {$azim[$i]= 90-$azi[$i]}
     else              {$azim[$i]=450-$azi[$i]}
     print TMP "2.12 2.12 $azim[$i] 1.55\n" } # $aoi[$i]\n"}
  close(TMP);
  $gmtstr .= "psxy $gmtp/azi.tmp -JX2.12/2.12 -R-2.12/2.12/-2.12/2.12 -Sv.05/.15/.15 ";
  $gmtstr .= "-G0/0/0 -Mnext -P -N -X15 -Y-3.5 -O -K >> $gmtp/$outps\n";
  $gmtstr .= "psmeca << EOF -R-2.12/2.12/-2.12/2.12 -JX2.12/2.12 -G150/150/150 -L ";
  $gmtstr .= "-Sc14.0 -N -O -K -C -X0 -Y0 >> $gmtp/$outps\n";
  $gmtstr .= "2.12 2.12 0 $pla[0] $pla[1] $pla[2] $pla[3] $pla[4] $pla[5] 1. 0 0 0\n";
  $gmtstr .= "EOF\n";

  ##### here loop over stations
  ### plot spectrum
  # -- parameters
  my $xback = 5;
  my $ydown = 0;
  my $minbegvalue=$frm; 
  my $maxbegvalue=$frM;
  my $MinAmp     =$Pwm;
  my $MaxAmp     =$PwM;
# my $minbegvalue=0.02; 
# my $maxbegvalue=1;
# my $MinAmp     =0.0000001;
# my $MaxAmp     =1;
  my $alt        ="2.5l";
  my $lar        ="5.5l";
  my $lh         = "";
  my $ls         = "WS";
  for(my $i=0; $i<=$#obsfiles;$i++) {

  #  if ($i== 0 || $i== 3 || $i== 6 || $i== 9 || $==12 || $i==15 || $i==18 ||
  #      $i==21 || $i==24 || $i==27 || $i==30 || $==33 || $i==36 || $i==39 ||
  #      $i==42 || $i==45 || $i==48 || $i==51 || $==54 || $i==57 || $i==60   ) {
     if ($i==0) {
         $xback=-13.0;
         $ydown=-3.0;
         $lh   = "PwSp";
         $ls   = "WS";
     }
     elsif(($i==3)  || ($i==6)  || ($i==9)  || ($i==12) || ($i==15) || ($i==18) ||
           ($i==21) || ($i==24) || ($i==27) || ($i==30) || ($i==33) || ($i==36)   ) {
         $xback=-12.0;
         $ydown=-4.0;
         $lh   = "PwSp";
         $ls   = "WS";
     }
     else {
         $xback=6.0;
         $ydown=0.0;
         $lh   = "";
         $ls   = "wS";
     }

  my $delta = sprintf("%.1f",$dis[$i]);
  my $azimu = sprintf("%.1f",$azi[$i]);
  my $takeo = sprintf("%.1f",$aoi[$i]);
  my $header = "$staz[$i] $comp[$i]     ";
     $header.= "@\360@ = $delta@\260@ ";
     $header.= "@\330@ = $azimu@\260@ ";
     $header.= "Aoi = $takeo@\260@"; 

  # Observed Black line
  $gmtstr.= "cat $obsf/$spfi[$i] | awk '{print \$2,\$4}' | ";
  $gmtstr.= "psxy -H -JX$lar/$alt -R$minbegvalue/$maxbegvalue/$MinAmp/$MaxAmp ";
  $gmtstr.= "-Ba1f3p::/a1f3p:\"$lh\"::.\"$header\":$ls ";
  $gmtstr.= "-W2 -P -O -K -X$xback -Y$ydown -W3 >> $gmtp/$outps\n";

  # Synthetic, thin dashed black line
  $gmtstr.= "cat $synp[$i]/$synsp[$i] | awk '{print \$2,\$4}' | ";
  $gmtstr.= "psxy -H -JX$lar/$alt -R$minbegvalue/$maxbegvalue/$MinAmp/$MaxAmp ";
  $gmtstr.= "-W2 -P -O -K -X0 -Y0 -W1t20_10:0/0/0/0 >> $gmtp/$outps\n";

  # perio range, thin red line
  my @T = split(/\_/,$periods[$i]);
  my $tm=sprintf("%.2e",$T[0]);
  my $tM=sprintf("%.2e",$T[1]);
  $gmtstr.= "cat $synp[$i]/$synsp[$i] | awk '{if(\$3 >= $tm && \$3 <= $tM) print \$2,\$4}' | ";
  $gmtstr.= "psxy -H -JX$lar/$alt -R$minbegvalue/$maxbegvalue/$MinAmp/$MaxAmp ";
  $gmtstr.= "-W2 -P -O -K -X0 -Y0 -W6t40_10:0/250/0/0 >> $gmtp/$outps\n";


  }


  ### Write end gmt
  $gmtstr .= "pstext << END -JX2.12 -R0/1/0/2 -P -O -N -X-12.0 -Y-10 >> $gmtp/$outps\n";
  $gmtstr .= "2 0.2 8 0 5 5 Random MT V4.0\n";
  $gmtstr .= "END\n";

# print "$gmtstr";
  system("$gmtstr");
  system("ps2pdf $gmtp/$outps $gmtp/$outpdf");
  system("open $gmtp/$outpdf");



 ##################################################################
 ##################################################################

 sub MakeNameOutfilePs {

     my @in =  @_;
     my @l = split(/\./,$in[0]);
     my @p = split(/\//,$l[0]);
     my $nr = sprintf("%05s",$NrMt);
     my $out1 = $p[$#p]."_".$z."_".$nr."_".$id.".ps";
     my $out2 = $p[$#p]."_".$z."_".$nr."_".$id.".pdf";

     return ($out1, $out2);

 }

 sub ReadStazPar {

     my $p = shift(@_);
     my @s = @_;
     for(my $i=0; $i<=$#s; $i++) {
        my @l = grep {/$s[$i]/} @inn;
        chomp($l[0]);
        my @f = split(' ',$l[0]);
        $s[$i] = $f[6]  if($p==0);  #get +1
        $s[$i] = $f[5]  if($p==1);
        $s[$i] = $f[10] if($p==2);
     } 
     return @s;

 }

 sub GetInfo {
  
     my $inf = $_[0];
     my $dat = $_[1];

     chomp($inf);
     my @l = split(' ',$inf);
     shift(@l);
     my $lat = shift(@l);
     my $lon = shift(@l);
     my $hyp = sprintf("%.1f",shift(@l));
     my $loc = join(' ',@l);

     chomp($dat);
     my @l = split(' ',$dat);
     my $yy = $l[0];
     my $mo = $l[1];
     my $dd = $l[2];
     my $hh = $l[3];
     my $mm = $l[4];
     my $ss = $l[5];

     return ($lat,$lon,$hyp,$loc,$yy,$mo,$dd,$hh,$mm,$ss);

 }

 sub FindParameters {

     my $i = shift(@_);
     my $s = shift(@_);
     my @ou;

     open(MT,"<$mtp"); my @mt=<MT>; close(MT);
     if($s==2) {
       chomp($mt[$i*10+$s]);
       my @l = split(' ',$mt[$i*10+$s]);
       shift(@l);
       @ou = @l;
     }
     if($s==4) {
       chomp($mt[$i*10+$s]);
       my @l = split(' ',$mt[$i*10+$s]);
       @ou=($l[2],$l[6]); 
     }
     if($s==5) {
       chomp($mt[$i*10+$s]);
       my @l = split(' ',$mt[$i*10+$s]);
       push(@ou,$l[3],$l[4],$l[5]);
       chomp($mt[$i*10+$s+1]);
       my @l = split(' ',$mt[$i*10+$s+1]);
       push(@ou,$l[2],$l[3],$l[4]);
     }
     if($s==7) {
       chomp($mt[$i*10+$s]);
       my @l = split(' ',$mt[$i*10+$s]);
       push(@ou,$l[3],$l[4],$l[5]);
       chomp($mt[$i*10+$s+1]);
       my @l = split(' ',$mt[$i*10+$s+1]);
       push(@ou,$l[2],$l[3],$l[4]);
       chomp($mt[$i*10+$s+2]);
       my @l = split(' ',$mt[$i*10+$s+2]);
       push(@ou,$l[2],$l[3],$l[4]);
     }
   
     return @ou;

 }

 sub FindSynSpFiles {

     my @in = @_;
     my @ou;
     my $dim = ($#in+1)/2;
     for(my $i=1; $i<=$dim;$i++) {
        my $k=$i-1;
        $ou[$k] = $in[$k].".".$libre.".".$mtnr.".".$z.".".$in[$k+$dim].".sp"; 
     }
     return @ou;
 }

 sub SelectSynPath {

     my @in = @_;
     for(my $i=0; $i<=$#in; $i++) {
        my @p = grep {/$in[$i]/} @alldirs;
        $in[$i] = $p[0];
     }
     return @in;
  
 }

 sub makeNameSpfiles {

     my @in = @_;
     for(my $i=0; $i<=$#in; $i++) {
       my @l = split(/\./,$in[$i]);
       $l[$#l] = "sp";
       $in[$i]=join('.',@l);
     }
     return @in;

 }

 ##################################################################

 sub ReadSac {

     my $pa = shift(@_);
     my $st = shift(@_);
     my @ou = ();

     open(FH,"<$pa/$st") or die; binmode(FH);
     my $npts  = ReadValue(79,4,'i');
     my $delta = ReadValue(0,4,'f');
     if($npts<$NmaxPoints) {
        for(my $i=158; $i<=158+$npts; $i++) {
           my $a=ReadValue($i,4,'f');
           push(@ou,$a);
        }
        for(my $i=0; $i<=$NmaxPoints-$npts-2; $i++) {
           push(@ou,0);
        }
     }
     if($npts>=$NmaxPoints) {
        for(my $i=158; $i<=158+$NmaxPoints-1; $i++) {
           my $a=ReadValue($i,4,'f');
           push(@ou,$a);
        }

     }
     return ($delta,@ou);

 }

 ##################################################################

 sub ReadValue {

   seek(FH, @_[0]*@_[1],0);
   read(FH, my $tmp, @_[1]);
   $tmp=unpack(@_[2], $tmp);
   return $tmp;

 }

 ##################################################################

 sub SelectComponents {

     my @in = @_;
     for(my $i=0; $i<=$#in; $i++) {
         my @l=split(/\./,$in[$i]);
         $in[$i]=$l[3];
     }
     return @in;

 }

 sub SelectStations {

     my @in = @_;
     for(my $i=0; $i<=$#in; $i++) {
         my @l=split(/\./,$in[$i]);
         $in[$i]=$l[0];
     }
     return @in;

 }

 ##################################################################

 sub MakeNameObs {

     my $sta = shift(@_);
     my $cha = shift(@_);
     my $con = shift(@_);
     my $yyy = shift(@_);
     my $data = substr($oblib,0,9);
     my $out;

     $out = $sta.".".$cha.".".$yyy.".".$con.".sac";


     return $out;

 }

 ##################################################################

 sub GetAllDirs {

     my $dic = shift(@_);
     my $z   = shift(@_);
     my @dir = @_;
     my @all = ();

     foreach $_ (@dir) {
        chomp($_);
        if("$_" eq "$z") {
#       my @tmp = split('',$_);
#       next if($#tmp!=$dic);
#       if("$tmp[0]" eq "0" && "$tmp[1]" eq "0" && $tmp[2] == $dept) {
        opendir(SBD,"$wrkp/$_"); my @sbd=readdir(SBD); close(SBD);
        for(my $j=0; $j<=$#sbd; $j++) {
           next if("$sbd[$j]" eq ".");
           next if("$sbd[$j]" eq "..");
           next if("$sbd[$j]" eq ".DS_Store");
           my @f = split(/\_/,$sbd[$j]);
           next if("$f[$#f]" ne "sp");

           push(@all,"$wrkp/$_/$sbd[$j]");
       }
    }
  }

  return (@all);

 }

