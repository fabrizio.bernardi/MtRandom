#!/usr/bin/perl

 ###############################################################################
 #                                                                             #
 # This script check the polarity of synthetics                                #
 #                                                                             #
 # Input: Sta Depth P_time L_sec(for polarity)                                 #
 #                                                                             #
 # Output: ID_file Depth Z R T N E (1=Up   -1=Down)                            #
 #                                                                             #
 #                                                                             #
 ###############################################################################

 use Env;
 use strict;
 use Math::Trig;
#use Statistics::Basic qw(:all);

 ###############################################################################
 # Read arguments
 (my $indir, my $sta, my $azi, my $aoi, my $rot) = @ARGV; 
 if($#ARGV != 4) {
   print "Usage: $0 path station azimuth takeoff_angle rotation\n";
   print "Usage: $0 /Users/fabrizio/MTs/work/1917/005/DBN_005 DBN 333.8401 45.631 -45\n";
   exit;
 }
 
#my $depth; my $recl; my $P_time; my $T_sec; my $k;
 my $rd   = 45.0/atan(1.0);

 ###############################################################################
 # Set path and variables
 my $mtp   = "/Users/fabrizio/MTs/lib";
 my $mtlib = "mtlib_3.1_05000.lib";
 my $NrMT  = 5000;
 my @f     = split(/\_/,$indir);
 my $z     = $f[$#f];

 ###############################################################################
 my $outname = "$sta.pol";
 
 ###############################################################################
 # Open MT_lib
 # Open MT dir and read files
 # Open out file
 open(MT,"<$mtp/$mtlib"); my @mt=<MT>;        close(MT);
 opendir(DIR,"$indir");     my @dir=readdir(DIR); close(DIR);
 open(OUT,">$indir/$outname");
 
 ###############################################################################
 # Now loop over @dir
 for(my $i=0; $i<=$NrMT; $i++) {

    my $id    = sprintf("%05d",$i);
    my $namef = "$sta.mtlib_3.$id.$z.z";

    # Get MT elements 
    my @mt_elements = getMtElements(sprintf("%f",$i));

    my $zp_pol              = getVertPolarity($aoi,$azi,@mt_elements);
    (my $np_pol,my $ep_pol) = getOrizPolarity($azi+$rot,$zp_pol);


    my $a = sprintf("%s %2d %2d %2d","$indir/$namef",$zp_pol,$np_pol,$ep_pol);
    print OUT "$a\n";

 }

 close(OUT);



#for(my $i=0; $i<=$#dir; $i++) {
#
#   chomp($dir[$i]);
#   #take ID parameters
#   next if("$dir[$i]" eq ".");
#   next if("$dir[$i]" eq "..");
#   my @f= split(/\./,$dir[$i]);
#   next if("$f[$#f]" eq "t");
#   next if("$f[$#f]" eq "r");
#   next if("$f[$#f]" eq "e");
#   next if("$f[$#f]" eq "n");
#   next if("$f[$#f-2]" ne "01199");

#   (my $nrmt, my $comp, my $spin) = GetID($dir[$i]);

#   next if($spin==0);

#   # Get MT elements 
#   my @mt_elements = getMtElements(sprintf("%f",$nrmt));

#   my $zp_pol              = getVertPolarity($aoi,$azi,@mt_elements);
#   (my $np_pol,my $ep_pol) = getOrizPolarity($azi+$rot,$zp_pol);


#   my $a = sprintf("%s %2d %2d %2d","$indir/$dir[$i]",$zp_pol,$np_pol,$ep_pol);
#   print OUT "$a\n";

#}

#close(OUT);

 ###############################################################################
 ###############################################################################

 sub getMtElements {

     my $nr = shift(@_);

     chomp($mt[$nr*10+3]);
     my @l = split(' ',$mt[$nr*10+3]);
     shift(@l);

     return @l;

 }

 ###############################################################################

 sub getVertPolarity {

     my $aoi   = shift(@_);
     my $theta = shift(@_);
     my $mzz = $_[0];
     my $mxx = $_[1];
     my $myy = $_[2];
     my $mxz = $_[3];
     my $myz = $_[4];
     my $mxy = $_[5];
     my $P;

     #compute Polarity 
     my $x = cos($theta/$rd)*sin($aoi/$rd);
     my $y = sin($theta/$rd)*sin($aoi/$rd);
     my $z = cos($aoi/$rd);
     my $polar = $x*$mxx*$x   +
                 2*$x*$mxy*$y +
                 2*$x*$mxz*$z +
                 2*$y*$myz*$z +
                 $y*$myy*$y   +
                 $z*$mzz*$z;

     $P = -1 if($polar<0);
     $P =  1 if($polar>0);
     $P =  0 if($polar==0);


     return $P;

 }

 ###############################################################################

 sub getOrizPolarity {

     (my $azi, my $z) = @_;
      my $n;
      my $e;

      if($azi==0 || $azi==90 || $azi==180 || $azi==270 || $azi==360) {
         $n=0;
         $e=0;
      }
      if($azi>0 && $azi<90) {
         $n=$z;
         $e=$z;
      }
      if($azi>90 && $azi<180) {
         $n=-$z;
         $e=$z;
      }
      if($azi>180 && $azi<270) {
         $n=-$z;
         $e=-$z;
      }
      if($azi>270 && $azi<360) {
         $n=$z;
         $e=-$z;
      }

      return ($n,$e);

 }

 ###############################################################################

 sub GetID {

     my $in = shift(@_);
     my @In = split(/\./,$in);
     my $spin = 0;
        $spin = 1 if("$In[$#In-0]" eq "z"); 
        $spin = 1 if("$In[$#In-0]" eq "e"); 
        $spin = 1 if("$In[$#In-0]" eq "n"); 
        $spin = 1 if("$In[$#In-0]" eq "r"); 
        $spin = 1 if("$In[$#In-0]" eq "t"); 
     return($In[$#In-2],$In[$#In-0],$spin);

 }

 ###############################################################################
